import React from 'react';

import { withStyles } from '@material-ui/core/styles';

import {
  Button,
  Card,
  FormControl,
  FormGroup,
  FormHelperText,
  FormLabel,
  Input,
  InputLabel,
} from '@material-ui/core';

import { connect } from 'react-redux';
import { Field, reduxForm } from 'redux-form';

import { signIn } from '../../actions';
import history from '../../history';

import CustomLink from '../widget/CustomLink';
import { DEFAULT_EMBLEM, DEFAULT_TITLE } from '../../shared/given';

const styles = theme => ({
  emblemContainer: {
    padding: `${theme.spacing(2)}px ${theme.spacing(10)}px 0`,
  },
  emblem: {
    width: '100%',
    height: 'auto',
  },
  authCard: {
    margin: 'auto',
    maxWidth: theme.spacing(60),
  },
  form: {
    margin: 'auto',
    padding: `${theme.spacing(2)}px ${theme.spacing(6)}px`,
    maxWidth: theme.spacing(60),
  },
  formLabel: {
    fontWeight: 700,
  },
  formControl: {
    marginBottom: theme.spacing(2),
  },
  buttonGroup: {
    textAlign: 'center',
  },
  button: {
    margin: theme.spacing(1),
  }
});

class AuthForm extends React.Component {
  state = {
    title: `Welcome to ${DEFAULT_TITLE}`,
  };

  onSubmit = values => {
    this.props.signIn(values.username, values.password)
      .then(
        () => {
          const { location } = this.props;
          if (location.state && location.state.from) {
            history.push(location.state.from.pathname);
          } else {
            history.push('/');
          }
        }
      )
      .catch(
        () => {
          this.props.reset();
          this.setState({
            title: 'Login failed, please retry!'
          });
        }
      );
  };

  renderTextInput = ({
    id,
    input,
    label,
    meta: {
      touched, error,
    },
    type,
    helperText,
  }) => {
    const { classes } = this.props;
    const inputId = `auth__${id}`;

    return (
      <FormControl
        className={classes.formControl}
        error={touched && !!error}
      >
        <InputLabel htmlFor={inputId}>
          {label}
        </InputLabel>
        <Input
          id={inputId}
          type={type}
          aria-describedby={`${inputId}__helper`}
          {...input}
        />
        <FormHelperText
          id={`${inputId}__helper`}
        >
          {
            touched
            ? error || 'Looks good!'
            : helperText
          }
        </FormHelperText>
      </FormControl>
    );
  };

  render() {
    const { classes, handleSubmit, submitting, invalid, anyTouched } = this.props;

    return (
      <Card
        className={classes.authCard}
      >
        <div className={classes.emblemContainer}>
          <img
            className={classes.emblem}
            src={DEFAULT_EMBLEM}
            alt={"Emblem"}
          />
        </div>
        <form
          onSubmit={handleSubmit(this.onSubmit)}
          className={classes.form}
        >
          <FormLabel
            className={classes.formLabel}
            component="legend"
            required
            error={anyTouched && invalid}
          >
            {this.state.title}
          </FormLabel>
          <FormGroup>
            <Field
              name="username"
              id="username"
              label="Username"
              component={this.renderTextInput}
              helperText="Your username"
            />
            <Field
              name="password"
              id="password"
              label="Password"
              type="password"
              component={this.renderTextInput}
              helperText="Your password"
            />
            <div className={classes.buttonGroup}>
              <Button
                className={classes.button}
                disableRipple={true}
                variant="contained"
                color="primary"
                type="submit"
                disabled={invalid || submitting}
              >
                Log In
              </Button>
              <Button
                className={classes.button}
                disableRipple={true}
                variant="contained"
                color="secondary"
                type="button"
                component={CustomLink}
                to="/signup"
              >
                Sign Up
              </Button>
            </div>
          </FormGroup>
        </form>
      </Card>
    );
  }
}

const validate = values => {
  const errrors = { };

  if (!values.username) {
    errrors.username = 'Required';
  }

  if (!values.password) {
    errrors.password = 'Required';
  }

  return errrors;
};

const StyledAuthForm = withStyles(styles)(AuthForm);

const ReduxAuthForm = reduxForm({
  form: 'AuthForm',
  validate,
})(StyledAuthForm);

const mapStateToProps = state => ({
  auth: state.auth
});

export default connect(mapStateToProps, {
  signIn
})(ReduxAuthForm);