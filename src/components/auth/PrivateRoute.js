import React from 'react';
import { Route, Redirect } from 'react-router-dom';

import { connect } from 'react-redux';

import LoadingSpinner from '../widget/Loading';
import { DEFAULT_TITLE } from '../../shared/given';

const PrivateRoute = ({ component: Component, auth, title, componentProps, ...rest }) => {
  return (
    <Route
      {...rest}
      render={
        props => (
          auth.isSignedIn
          ? <Component {...props} {...componentProps} title={title || DEFAULT_TITLE} />
          : auth.isSignedIn === false
            ? <Redirect to={{ pathname: '/auth', state: { from: props.location } }} />
            : <LoadingSpinner />
        )
      }
    />
  );
};

const mapStateToProps = state => ({
  auth: state.auth
});

export default connect(mapStateToProps, null, null, { pure: false })(PrivateRoute);