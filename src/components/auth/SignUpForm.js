import React from 'react';

import { withStyles } from '@material-ui/core/styles';

import {
  Button,
  Card,
  FormControl,
  FormGroup,
  FormHelperText,
  FormLabel,
  Input,
  InputLabel,
} from '@material-ui/core';

import { Field, reduxForm } from 'redux-form';
import { connect } from 'react-redux';

import { signUp } from '../../actions';
import history from '../../history';

const styles =  theme => ({
  cardContainer: {
    margin: 'auto',
    padding: `${theme.spacing(2)}px ${theme.spacing(4)}px`,
    maxWidth: theme.spacing(72),
  },
  form: {
    margin: 'auto',
    padding: `${theme.spacing(2)}px ${theme.spacing(6)}`,
    maxWidth: theme.spacing(72),
  },
  formControl: {
    marginBottom: theme.spacing(2),
  },
  button: {
    margin: theme.spacing(0.5),
    fontFamily: "Lucida Sans Unicode, Lucida Grande, sans-serif",
  },
  buttonGroup: {
    textAlign: 'center',
  },
});

class SignUpForm extends React.Component {
  state = {
    title: 'Please fill up this form'
  };

  onSubmit = values => {
    this.props.signUp(values)
      .then(
        ret => history.push('/')
      )
      .catch(
        err => {
          const title = err.errmsg || 'Sign up failed, please try again';
          this.setState({
            title
          });
        }
      );
  };

  renderTextInput = ({
    input,
    label,
    type,
    required,
    inputId,
    helperText,
    meta: {
      touched, error
    },
  }) => {
    const { classes } = this.props;
    const req = required || false;

    return (
      <FormControl
        className={classes.formControl}
        error={touched && !!error}
      >
        <InputLabel
          htmlFor={inputId}
          required={req}
        >
          {label}
        </InputLabel>
        <Input
          id={inputId}
          aria-describedby={`${inputId}__helper`}
          {...input}
          type={type || "text"}
        />
        <FormHelperText
          id={`${inputId}__helper`}
        >
          {
            touched
            ? error || 'Looks good!'
            : `${helperText}`
          }
        </FormHelperText>
      </FormControl>
    );
  };

  renderBio = () => {
    return null;
  };

  render() {
    const { classes, handleSubmit, pristine, reset, submitting, invalid, anyTouched } = this.props;

    return (
      <Card
        className={classes.cardContainer}
      >
        <form
          onSubmit={handleSubmit(this.onSubmit)}
          className={classes.form}
        >
          <FormLabel
            component="legend"
            required
            error={anyTouched && invalid}
          >
            {this.state.title}
          </FormLabel>
          <FormGroup>
            <Field
              name="username"
              component={this.renderTextInput}
              label="Username"
              inputId="signup__username"
              helperText="The name that is used to login"
              required
            />
            <Field
              name="password"
              component={this.renderTextInput}
              label="Password"
              type="password"
              inputId="signup__password"
              helperText="Your password"
              required
            />
            <Field
              name="rePassword"
              component={this.renderTextInput}
              label="Password Validatior"
              type="password"
              inputId="signup__re-password"
              helperText="Please re-enter your password"
              required
            />
            <Field
              name="realName"
              component={this.renderTextInput}
              label="Name"
              inputId="signup__realname"
              helperText="Should be your real name"
              required
            />
            <Field
              name="email"
              component={this.renderTextInput}
              label="Email"
              inputId="signup__email"
              helperText="Your email"
              required
            />
            <Field
              name="nickname"
              component={this.renderTextInput}
              label="Nickname"
              inputId="signup__nickname"
              helperText="Your nickname"
            />
            <Field
              name="biography"
              component={this.renderTextInput}
              label="Bio"
              inputId="signup__bio"
              helperText="A few lines of your biography"
            />
          </FormGroup>
          <div className={classes.buttonGroup}>
            <Button
              disableRipple={true}
              variant="contained"
              color="primary"
              type="submit"
              disabled={invalid || submitting}
              className={classes.button}
            >
              Sign Up
            </Button>

            <Button
              disableRipple={true}
              variant="contained"
              color="secondary"
              disabled={pristine || submitting}
              onClick={reset}
              className={classes.button}
            >
              Clear Data
            </Button>
          </div>
        </form>
      </Card>
    );
  }
}

const validate = values => {
  const errors = {};
  if (!values.realName) {
    errors.realName = 'Required';
  } else if (values.realName.length < 3) {
    errors.realName = 'Your name is too short!';
  } else if (values.realName.length > 20) {
    errors.realName = 'Your name is too long!';
  }

  if (!values.password) {
    errors.password = 'Required';
  } else if (values.password.length < 6) {
    errors.password = 'Your password is too short!';
  } else if (values.password.length > 20) {
    errors.password = 'Your password is too long!';
  }

  if (values.rePassword !== values.password) {
    errors.rePassword = 'These passwords are not matched.';
  }

  if (!values.username) {
    errors.username = 'Required';
  } else if (values.username.length < 6) {
    errors.username = 'The username is too short!';
  } else if (values.username.length > 20) {
    errors.username = 'The username is too long!';
  } else if (!/^[a-zA-Z0-9_.]+/.test(values.username)) {
    errors.username = 'The username contains illegal characters.';
  }

  if (!values.email) {
    errors.email = 'Required';
  } else if (!/^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+.[a-zA-Z0-9-.]+$/.test(values.email)) {
    errors.email = 'Invalid email address.';
  }

  return errors;
};

const StyledSignUpForm = withStyles(styles)(SignUpForm);

const ReduxSignUpForm = reduxForm({
  form: 'SignUpForm',
  validate,
})(StyledSignUpForm);

const mapStateToProps = state => ({
  auth: state.auth
});

export default connect(mapStateToProps, {
  signUp: signUp
})(ReduxSignUpForm);