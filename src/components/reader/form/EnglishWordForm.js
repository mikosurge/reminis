import React from 'react';

import { withStyles } from '@material-ui/core/styles';

import { Field, reduxForm } from 'redux-form';

import {
  Button,
  Divider,
  FormGroup,
  FormLabel,
  IconButton,
} from '@material-ui/core';

import AddIcon from '@material-ui/icons/Add';


const styles = theme => ({
  container: {
    minWidth: theme.spacing(60),
  },
  form: {
    margin: 'auto',
    padding: theme.spacing(1),
    minWidth: theme.spacing(15),
    maxWidth: theme.spacing(60),
  },
  formTitle: {
    fontWeight: 700,
    marginBottom: theme.spacing(2),
    fontSize: theme.typography.pxToRem(18),
  },
  buttonGroups: {
    marginTop: theme.spacing(2),
    textAlign: 'center',
  },
  button: {
    margin: theme.spacing(0.5),
  },
});


const validate = values => {
  const errors = { };

  return errors;
};


class EnglishWordForm extends React.Component {
  state = {

  };

  onSubmit = formValues => {
    if (typeof this.props.onSubmit === 'function') {
      this.props.onSubmit(formValues);
    }
  };

  handleChange = fieldName => event => {
    let newValue = null;
    if (event.target.type === 'checkbox') {
      newValue = event.target.value !== 'true';
    } else {
      newValue = event.target.value;
    }

    this.props.change(fieldName, newValue);
    this.props.onChange(fieldName, newValue):
  };

  handleChangeExample = fieldName => event => {
    let newValue = null;

    this.props.change('examples', newValue);
    this.props.onChange('examples', newValue);
  };

  render() {
    const {
      handleSubmit,
      pristine,
      reset,
      submitting,
      invalid,
      anyTouched,
      classes,
    } = this.props;

    const {
      nested,
      editting,
    } = this.props;

    const {
      wordType,
      word,
      pronunciation,
      meaningSource,
      meaning,
      examples,
      relatives,
      groups,
      alternate,
      origin,
    } = this.props.word;

    return (
      <div className={classes.container}>
        <form
          className={classes.form}
          onSubmit={handleSubmit(this.onSubmit)}
        >
          {!nested &&
            <FormLabel
              className={classes.formTitle}
              component="legend"
              required
              error={anyTouched && invalid}
            >
              Create/Edit an English Word
            </FormLabel>
          }

          <FormGroup>
            <Field
              variant="outlined"
              name="word"
              component={FormTextField}
              id="word"
              label="word"
              helperText="Enter the English word"
              required
              handleChange={editting ? null : this.handleChange}
              disabled={editting}
            />
            <Divider />
            <Field
              name="wordType"
              component={FormSelect}
              id="wordType"
              label="Type"
              helperText="The word type"
              required
              handleChange={this.handleChange}
              controlledValue={wordType}
            />
            <Field
              name="pronunciation"
              component={FormTextField}
              id="pronunciation"
              label="Pronunciation"
              helperText="How it is pronouced"
              handleChange={this.handleChange}
              controlledValue={pronunciation}
              required
            />
            <Field
              name="meaningSource"
              component={FormSelect}
              id="meaningSource"
              label="Source"
              helperText="The meaning source"
              handleChange={this.handleChange}
              controlledValue={meaningSource}
              required
            />
            <Field
              name="meaning"
              component={FormTextArea}
              rows={3}
              id="meaning"
              label="Meaning"
              helperText="The word meaning"
              handleChange={this.handleChange}
              controlledValue={meaning}
              required
            />
            <div className={classes.exampleBlock}>
              {examples.map(
                (example, i) => (
                  <Field
                    key={`example${i}`}
                    name={`example${i}`}
                    component={FormTextArea}
                    rows={3}
                    id={`example${i}`}
                    label={`Example ${i}`}
                    helperText={null}
                    handleChange={this.handleChangeExample}
                    controlledValue={example}
                    required
                  />
                )
              )}
              <IconButton>
                <AddIcon color="primary" />
              </IconButton>
            </div>
            <Field
              name="groups"
              component={FormTextField}
              id="groups"
              label="Groups"
              helperText="Groups that the word belongs to"
              handleChange={this.handleChangeGroups}
              controlledValue={groups.join(', ')}
              required
            />
            <Field
              name="alternate"
              component={FormTextField}
              id="alternate"
              label="Alternate"
              helperText="The alternate in your native language"
              handleChange={this.handleChange}
              controlledValue={alternate}
              required
            />
            <Field
              name="origin"
              component={FormSelect}
              id="origin"
              label="Origin"
              helperText="Where the word has been founded"
              handleChange={this.handleChange}
              controlledValue={origin || "Sisters"}
              required
            />
          </FormGroup>
        </form>
      </div>
    )
  }
}