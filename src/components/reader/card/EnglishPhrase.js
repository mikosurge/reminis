import React from 'react';

import { makeStyles } from '@material-ui/core/styles';

import {
  Card,
} from '@material-ui/core';

import {
  Edit,
  Delete,
} from '@material-ui/icons';


const useStyles = makeStyles(theme => ({
  container: {

  },
}));


const EnglishPhrase = props => {
  const classes = useStyles();

  return (
    <Card className={classes.container}>
      {`Just an English Phrase`}
    </Card>
  );
};

export default EnglishPhrase;