import React from 'react';

import { makeStyles } from '@material-ui/core/styles';

import {
  Box,
  Card,
  CardActions,
  CardContent,
  CardHeader,
  Divider,
  IconButton,
  Typography,
} from '@material-ui/core';

import {
  Edit,
  Delete,
} from '@material-ui/icons';


const useStyles = makeStyles(theme => ({
  container: {
    border: '1px solid #DDD',
  },
  top: {
    padding: theme.spacing(1.5, 2, 1),
    display: 'flex',
    flexDirection: 'row',
  },
  header: {
    flexGrow: 1,
  },
  subheader: {
    display: 'flex',
    flexDirection: 'row',
  },
  delim: {
    margin: theme.spacing(0, 0.5),
  },
  bottom: {
    padding: theme.spacing(1, 2, 0.5),
    '&:last-child': {
      paddingBottom: theme.spacing(1.5),
    },
  },
  content: {
    lineHeight: 1.5,
  },
  helperIcons: {
    display: 'flex',
    alignItems: 'flex-start',
    padding: 0,
  },
  helperIcon: {
    padding: theme.spacing(0.5),
  },
  divider: {

  },
  meaning: {

  },
  examples: {
    padding: theme.spacing(1, 2, 0.5),
    '&:last-child': {
      paddingBottom: theme.spacing(1.5),
    },
  },
  example: {
    paddingLeft: theme.spacing(2),
    borderLeft: '5px solid #a0aabf',
    fontStyle: 'italic',
    margin: theme.spacing(1, 0),
  },
  relevant: {
    padding: theme.spacing(1, 2, 0.5),
    '&:last-child': {
      paddingBottom: theme.spacing(1.5),
    },
    display: 'flex',
    flexDirection: 'row',
  },
  group: {
    flexGrow: 1,
  },
  origin: {
    flexGrow: 1,
    textAlign: 'right',
  }
}));


const EnglishWord = props => {
  const classes = useStyles();
  const { word } = props;

  return (
    <>
      <Card className={classes.container}>
        <Box className={classes.top}>
          <Box className={classes.header}>
            <Typography
              variant="h4"
            >
              cognition
            </Typography>
            <Box className={classes.subheader}>
              <Typography
                variant="subtitle2"
              >
                noun
              </Typography>
              <Box className={classes.delim}>
                •
              </Box>
              <Typography
                variant="subtitle2"
              >
                /kɒɡˈnɪʃ.ən/
              </Typography>
            </Box>
          </Box>
          <CardActions
            classes={{
              root: classes.helperIcons
            }}
          >
            <IconButton
              className={classes.helperIcon}
              color="primary"
            >
              <Edit />
            </IconButton>
            <IconButton
              className={classes.helperIcon}
              color="secondary"
            >
              <Delete />
            </IconButton>
          </CardActions>
        </Box>
        <Divider />
        <CardContent classes={{ root: classes.bottom }}>
          <Box
            className={classes.meaning}
          >
            <Typography variant="subtitle1" className={classes.content}>
              the use of conscious mental processes
            </Typography>
          </Box>
        </CardContent>
        <CardContent
          className={classes.examples}
        >
          <Box className={classes.example}>
            a book on human learning, memory, and cognition
          </Box>
        </CardContent>
        <CardContent classes={{ root: classes.relevant }}>
          <Box className={classes.group}>
            <Typography variant="subtitle2" style={{ textDecoration: 'underline', fontWeight: 700 }}>
              {'Science of psychology & psychoanalysis'}
            </Typography>
          </Box>
          <Box className={classes.origin}>
            <Typography variant="subtitle2" style={{ fontStyle: 'italic' }}>
              Found in Reminis
            </Typography>
          </Box>
        </CardContent>
      </Card>
    </>
  );
};

export default EnglishWord;
