import React from 'react';

import { makeStyles } from '@material-ui/core/styles';

import {
  Button,
  ButtonBase,
  IconButton,
  Divider,
  Grid,
  Hidden,
  Paper,
  Tooltip,
  Typography,
} from '@material-ui/core';

import Markdown from '../../widget/Markdown';
import {
  AccountCircle,
  Person,
  History,
  HourglassEmpty,
  NearMe,
  Replay,
  Settings,
} from '@material-ui/icons';


const useStyles = makeStyles(theme => ({
  container: {
    height: '100%',
    padding: theme.spacing(0, 1),
  },
  header: {
    display: 'flex',
    flexDirection: 'row',
    alignItems: 'flex-start',
    marginBottom: theme.spacing(0.5),
    // position: 'sticky',
    // top: 0,
    padding: theme.spacing(0, 2),
  },
  headerInfo: {
    display: 'flex',
    flexDirection: 'column',
    flexGrow: 1,
  },
  headerTop: {
    display: 'flex',
    flexDirection: 'row',
    justifyContent: 'space-between',
    width: '80%',
    marginBottom: theme.spacing(0.5),
    maxHeight: theme.spacing(4),
  },
  headerBottom: {
    display: 'flex',
    flexDirection: 'row',
    justifyContent: 'space-between',
    width: '80%',
    marginBottom: theme.spacing(0.5),
    paddingLeft: theme.spacing(1),
    maxHeight: theme.spacing(3),
  },
  headerField: {
    display: 'flex',
    flexDirection: 'row',
    alignItems: 'center',
  },
  headerIcon: {
    marginRight: theme.spacing(0.5),
    color: theme.palette.primary.main,
  },
  headerFieldText: {
    fontFamily: 'Arial, Helvetica, sans-serif',
  },
  headerOriginText: {
    fontWeight: 700,
    marginRight: theme.spacing(1),
  },
  headerIconBig: {
    marginRight: theme.spacing(1),
    color: theme.palette.primary.main,
    fontSize: theme.typography.pxToRem(32),
  },
  headerNavis: {
    display: 'flex',
    flexDirection: 'row',
  },
  naviButton: {
    width: '32px',
    height: '32px',
    borderRadius: '32px',
    margin: theme.spacing(0, 0.5),
    '&:first-child': {
      marginLeft: 0,
    },
    '&:last-child': {
      marginRight: 0,
    },
  },
  naviImg: {
    borderRadius: '32px',
  },
  reloadButton: {
    backgroundColor: theme.palette.secondary.main,
    color: 'white',
    width: '48px',
    height: '48px',
    padding: 0,
    margin: theme.spacing(0, 0.5),
    '&:first-child': {
      marginLeft: 0,
    },
    '&:last-child': {
      marginRight: 0,
    },
    '&:hover': {
      backgroundColor: 'white',
      color: theme.palette.secondary.main,
      border: `2px solid ${theme.palette.secondary.main}`,
    },
  },
  settingsButton: {
    backgroundColor: theme.palette.primary.main,
    color: 'white',
    width: '48px',
    height: '48px',
    padding: 0,
    margin: theme.spacing(0, 0.5),
    '&:first-child': {
      marginLeft: 0,
    },
    '&:last-child': {
      marginRight: 0,
    },
    '&:hover': {
      backgroundColor: 'white',
      color: theme.palette.primary.main,
      border: `2px solid ${theme.palette.primary.main}`,
    }
  },
  reloadIcon: {
    fontSize: theme.typography.pxToRem(32),
  },
  settingsIcon: {
    fontSize: theme.typography.pxToRem(32),
  },
  textPaper: {
    height: '100%',
    // overflow: 'overlay',
    padding: theme.spacing(2, 0, 0),
    marginRight: theme.spacing(0.5),
    border: '1px solid #DDD',
  },
  imagePaper: {
    maxHeight: '100%',
    overflow: 'overlay',
    padding: theme.spacing(2, 4, 2, 2),
    marginLeft: theme.spacing(0.5),
  },
  markdown: {
    maxHeight: 'calc(100% - 64px - 4px - 1px)',
    overflow: 'overlay',
    padding: theme.spacing(0, 6, 0, 4),
  },
  divider: {
    backgroundColor: '#BBB',
  },
  tooltip: {
    fontSize: theme.typography.pxToRem(14),
  }
}));

const defaultOptions = {
  // splitContent: true,
};


const QUICK_SPEED = 275;
const NORMAL_SPEED = 175;


const PostView = props => {
  const classes = useStyles();
  const ref = React.createRef();
  const { post, options, onReload, onSettings } = props;
  const {
    _id,
    origin,
    translationOrigin,
    translator,
    category,
    _collection,
    message,
    images,
    tags,
    remainingImages,
    timestamp,
    timestampUTC,
    textContent,
    imageContent,
    readCount,
  } = post;

  const _options = options || defaultOptions;
  const {
    splitContent,
  } = _options;
  let _splitContent = splitContent !== undefined ? splitContent : images.length > 1;

  const handleReload = () => {
    ref.current.scrollTo(0, 0);
    onReload();
  }

  const generateReadTime = (content, nImages = 0) => {
    let wc = content.split(' ').length;
    let contentMins = wc > 2000 ? wc / QUICK_SPEED : wc / NORMAL_SPEED;
    let roundedMins = Math.ceil(contentMins + nImages * 0.2);
    return `${wc} words (${roundedMins} min${roundedMins > 1 ? 's' : ''})`;
  };

  return (
    <Grid
      container
      direction="row"
      className={classes.container}
    >
      <Hidden only={['xs']}>
        {!_splitContent &&
          <Grid item md={2} />
        }
      </Hidden>
      <Grid
        item
        sm={12}
        // md={!_splitContent ? 12 : 8}
        // lg={!_splitContent ? 12 : 9}
        md={8}
        style={{ height: '100%' }}
      >
        <Paper
          className={classes.textPaper}
        >
          <div className={classes.header}>
            <div className={classes.headerInfo}>
              <div className={classes.headerTop}>
                <div className={classes.headerField}>
                  <AccountCircle className={classes.headerIconBig} />
                  <Typography className={classes.headerFieldText} variant="h6" style={{ fontWeight: 700 }}>
                    {translator}
                  </Typography>
                </div>
                {_collection &&
                  <div className={classes.headerCollection}>
                    <Typography className={classes.headerFieldText} variant="subtitle1">
                      {`in ${_collection}`}
                    </Typography>
                  </div>
                }
                <div className={classes.headerField}>
                  <Typography className={classes.headerOriginText} variant="subtitle1">
                    {'Original:'}
                  </Typography>
                  <ButtonBase
                    className={classes.naviButton}
                    component="a"
                    href={translationOrigin}
                    target="_blank"
                  >
                    <img className={classes.naviImg} src={`/facebook.png`} alt="facebook" />
                  </ButtonBase>
                  {origin &&
                    <ButtonBase
                      className={classes.naviButton}
                      component="a"
                      href={origin}
                      target="_blank"
                    >
                      <img className={classes.naviImg} src={`/${category}.png`} alt="quora" />
                    </ButtonBase>
                  }
                </div>
              </div>
              <div className={classes.headerBottom}>
                <div className={classes.headerField}>
                  <History className={classes.headerIcon} />
                  <Typography className={classes.headerFieldText} variant="subtitle2">
                    {timestampUTC}
                  </Typography>
                </div>
                <div className={classes.headerField}>
                  <HourglassEmpty className={classes.headerIcon} />
                  <Typography className={classes.headerFieldText} variant="subtitle2">
                    {generateReadTime(textContent, images?.length)}
                  </Typography>
                </div>
                <div className={classes.headerField}>
                  <NearMe className={classes.headerIcon} />
                  <Typography className={classes.headerFieldText} variant="subtitle2">
                    {readCount || 0}
                  </Typography>
                </div>
              </div>
            </div>

            <div className={classes.headerNavis}>
              <Tooltip classes={{ tooltip: classes.tooltip }} placement="bottom-start" title="Another read">
                <IconButton
                  className={classes.reloadButton}
                  onClick={handleReload}
                >
                  <Replay className={classes.reloadIcon} />
                </IconButton>
              </Tooltip>
              <IconButton
                className={classes.settingsButton}
                onClick={onSettings}
                disabled
              >
                <Settings className={classes.settingsIcon} />
              </IconButton>
            </div>
          </div>
          <Divider className={classes.divider} />
          <div ref={ref} className={classes.markdown}>
            <Markdown source={textContent} />
            <Markdown source={imageContent} />
          </div>
        </Paper>
      </Grid>

      {_splitContent &&
        <Grid
          item
          sm={12}
          md={4}
          style={{ height: '100%' }}
        >
          <Paper
            className={classes.imagePaper}
          >
            <Markdown source={imageContent} />
          </Paper>
        </Grid>
      }
    </Grid>
  );
};

export default PostView;

