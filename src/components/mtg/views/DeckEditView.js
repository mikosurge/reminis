import React from 'react';
import { connect } from 'react-redux';

import { withStyles, createMuiTheme } from '@material-ui/core/styles';
import { ThemeProvider } from '@material-ui/styles';

import {
  Button,
  Card,
  Container,
  FormControl,
  FormHelperText,
  Grid,
  Input,
  InputLabel,
  LinearProgress,
  Paper,
  Tab,
  Tabs,
  Typography,
} from '@material-ui/core';
import FilterIcon from '@material-ui/icons/Filter';
import FilterListIcon from '@material-ui/icons/FilterList';
import SearchIcon from '@material-ui/icons/Search';

import CardTitle from '../card/CardTitle';
import CompactCardItem from '../card/CompactCardItem';
import CardSymbol from '../common/CardSymbol';
import CardLayout from '../card/CardLayout';
import FakeContent from '../../widget/FakeContent';
import NotificationBar from '../../widget/NotificationBar';
import {
  fetchCards,
  fetchDeck,
  fetchCollection,
  editChanges,
  submitDeckChanges,
  clearChanges,
} from '../../../actions';

import { extremelySortCards } from '../../../shared/sorting';
import { TabPanel, a11yProps } from '../../widget/TabUtilities';

import {
  REFINE_BLOCK,
  UTILITY_BLOCK,
  LIST_BLOCK,
} from '../../../shared/given';
import LoadingSpinner from '../../widget/Loading';

const { SEARCH, FILTER } = REFINE_BLOCK;
const { RESULT, VIEW } = UTILITY_BLOCK;
const { MAIN, SIDE } = LIST_BLOCK;

const theme = createMuiTheme({
  overrides: {
    MuiTabs: {
      flexContainer: {
        display: 'flex',
        justifyContent: 'flex-end',
      },
    },
  },
});


const styles = theme => ({
  container: {
    padding: `0 ${theme.spacing(1)}px`,
  },
  gridContainer: {

  },
  cardRefine: {
    margin: `${theme.spacing(1)}px 0`,
    padding: `${theme.spacing(1)}px ${theme.spacing(2)}px`,
  },
  formControl: {
    width: '100%',
  },
  paperResult: {
    margin: `${theme.spacing(1)}px 0`,
    padding: theme.spacing(1),
  },
  paperDeck: {
    margin: `${theme.spacing(1)}px 0`,
    padding: theme.spacing(1),
  },
  paperDeckButtons: {
    display: 'flex',
    flexDirection: 'row',
    justifyContent: 'flex-end',
  },
  paperDeckButton: {
    fontSize: '0.8rem',
    padding: `0 ${theme.spacing(1)}px`,
  },
  paperDeckSummary: {
    margin: `${theme.spacing(1)}px 0`,
    padding: theme.spacing(1),
    display: 'flex',
    flexDirection: 'row',
    border: '1px solid #AAA',
  },
  paperDeckImage: {
    width: '50%',
    maxHeight: theme.spacing(16),
    padding: theme.spacing(1),
  },
  paperDeckContent: {
    width: '50%',
    textAlign: 'right',
    padding: theme.spacing(1),
    display: 'flex',
    flexDirection: 'column',
    justifyContent: 'space-evenly',
  },
  paperDeckName: {
    lineHeight: 1.2,
  },
  paperDeckColors: {

  },
  paperCardList: {
    padding: `${theme.spacing(1)}px ${theme.spacing(2)}px`,
  },
  innerImage: {
    maxWidth: '100%',
    maxHeight: '100%',
  },
  utilityPaper: {
    padding: `${theme.spacing(1)}px ${theme.spacing(2)}px`,
  },
  tabContainer: {
    flexGrow: 1,
    width: '100%',
    // backgroundColor: theme.palette.background.paper,
    minHeight: theme.spacing(4),
    marginBottom: theme.spacing(1),
  },
  tabItem: {
    minWidth: theme.spacing(8),
    minHeight: theme.spacing(4),
    padding: 0,
    // backgroundColor: '#EEE',
    fontWeight: 700,
  },
  flexContainer: {
    display: 'flex',
    justifyContent: 'flex-end',
  },
});


class DeckEditView extends React.Component {
  state = {
    keyword: '',
    searchResults: [],
    isSubmitting: false,
    openSnackbar: false,
    snackbarProps: {
      variant: 'error',
      message: '',
    },
    refineTabIndex: 0,
    utilityTabIndex: 0,
    listTabIndex: 0
  };

  componentDidMount() {
    const { deckId } = this.props.match.params;
    const {
      clearChanges,
      fetchCards,
      fetchCollection,
      fetchDeck,
      imaginary,
    } = this.props;
    clearChanges();
    if (imaginary) {
      fetchCards()
        .catch(
          error => console.log(error)
        );
    } else {
      fetchCollection()
        .catch(
          error => console.log(error)
        );
    }
    fetchDeck(deckId)
      .then(
        result => { }
      )
      .catch(
        error => console.log(error)
      )
  }

  performSearch = (kw, isSubmit) => {
    const { imaginary, cards, collection, deck } = this.props;
    const cardSet = imaginary ? cards : collection.cards;

    kw = kw.toLowerCase();
    if (kw.length < 5 && !isSubmit) {
      this.setState({
        searchResults: []
      });
    } else {
      let tempResults = [];
      cardSet.forEach(
        unit => {
          let card = null;
          if (imaginary) {
            card = unit;
          } else {
            card = unit.card;
          }

          if (card.name.toLowerCase().includes(kw)) {
            if (imaginary) {
              tempResults.push({ card, num: { foil: 0, normal: 0 } });
            } else {
              tempResults.push(unit);
            }
          }
        }
      );

      if (imaginary) {
        tempResults = this.addNumber(tempResults, deck.cards);
      }

      this.setState({
        searchResults: tempResults,
      });
    }
  };

  // For imaginary deck only
  addNumber = (searchResults, cards) => {
    if (!searchResults) {
      return [];
    }

    if (!cards.length) {
      return searchResults;
    }

    cards.forEach(
      ({ card, foil, num }) => {
        searchResults.some(
          unit => {
            if (unit.card._id === card._id) {
              if (foil) {
                unit.num.foil = num;
              } else {
                unit.num.normal = num;
              }
              return true;
            }
            return false;
          }
        )
      }
    );

    return searchResults;
  }

  onSearchSubmit = e => {
    e.preventDefault();
    this.performSearch(this.state.keyword, true);
  };

  onSearchChange = ({ target: { value }}) => {
    this.setState({
      keyword: value,
    });

    this.performSearch(value, false);
  };

  onAddToChanges = ({ card, foil }) => {
    const { imaginary, collection, deck: { cards }, changes, editChanges } = this.props;
    const { searchResults } = this.state;
    const isMainSide = this.state.listTabIndex === MAIN;

    let currentInMain = 0;
    let currentInSide = 0;
    let existing = false;
    let originalNumber = null;

    if (!imaginary) {
      collection.cards.some(
        unit => {
          if (unit.card._id === card._id && unit.foil === foil) {
            originalNumber = unit.num;
            return true;
          }
          return false;
        }
      );
    }

    cards.forEach(
      unit => {
        if (unit.card._id === card._id && unit.foil === foil) {
          if (unit.isMain === isMainSide) {
            existing = true;
          }

          if (unit.isMain) {
            currentInMain += unit.num;
          } else {
            currentInSide += unit.num;
          }
        }
      }
    );

    changes.forEach(
      unit => {
        if (unit.card._id === card._id && unit.foil === foil) {
          if (unit.isMain) {
            currentInMain += unit.num;
          } else {
            currentInSide += unit.num;
          }
        }
      }
    );

    if (!imaginary && currentInMain + currentInSide + 1 > originalNumber) {
      console.log(`Limit exceeded`);
      return;
    }

    let perform = existing ? 'update' : 'insert';
    console.log(`Gonna ${perform} 1 ${card.name} foil=${foil} isMain=${isMainSide}`);
    editChanges({
      card,
      foil,
      num: 1,
      perform,
      isMain: isMainSide,
    });
  };

  onSubstractFromChanges = ({ card, foil }) => {
    const { collection, deck: { cards }, changes, editChanges } = this.props;
    const isMainSide = this.state.listTabIndex === MAIN;

    let currentInMain = 0;
    let currentInSide = 0;
    let perform = null;

    cards.forEach(
      unit => {
        if (unit.card._id === card._id && unit.foil === foil) {
          if (unit.isMain) {
            currentInMain += unit.num;
          } else {
            currentInSide += unit.num;
          }
        }
      }
    );

    changes.forEach(
      unit => {
        if (unit.card._id === card._id && unit.foil === foil) {
          if (unit.isMain) {
            currentInMain += unit.num;
          } else {
            currentInSide += unit.num;
          }
        }
      }
    );

    if (isMainSide) {
      if (currentInMain < 1) {
        return;
      }
      perform = currentInMain === 1 ? 'remove' : 'update';
    } else {
      if (currentInSide < 1) {
        return;
      }
      perform = currentInSide === 1 ? 'remove' : 'update';
    }

    console.log(`Gonna ${perform} 1 ${card.name} foil=${foil} isMain=${isMainSide}`);
    editChanges({
      card,
      foil,
      num: -1,
      perform,
      isMain: isMainSide,
    });
  };

  onSubmitChanges = e => {
    const { imaginary, submitDeckChanges, fetchDeck, clearChanges, deck: { _id } } = this.props;
    this.setState({ isSubmitting: true });
    submitDeckChanges(_id)
      .then(
        () => {
          return fetchDeck(_id);
        }
      )
      .then(
        () => {
          clearChanges();
          this.triggerNotification({
            variant: 'success',
            message: 'Successfully submitted!',
          });

          if (imaginary) {
            this.performSearch(this.state.keyword, true);
          }
        }
      )
      .catch(
        error => this.triggerNotification({
          variant: 'error',
          message: error.errmsg || error,
        })
      )
      .finally(
        () => this.setState({ isSubmitting: false })
      );
  };

  onClearChanges = e => {
    this.props.clearChanges();
  };

  calculateRemainings = ({ card, foil, num }) => {
    const { imaginary, deck, changes } = this.props;
    if (imaginary) {
      const _num = JSON.parse(JSON.stringify(num));
      changes.forEach(
        unit => {
          if (unit.card._id === card._id) {
            if (unit.foil) {
              _num.foil += unit.num;
            } else {
              _num.normal += unit.num;
            }
          }
        }
      );

      if (_num.foil < 0 || _num.normal < 0) {
        console.log(`${card.name} has its current number of ${JSON.stringify(_num)}`);
      }

      return _num;
    } else {
      const tempDeck = this.mergeWithChanges(deck.cards, changes);
      let result = num;
      tempDeck.forEach(
        unit => {
          if (unit.card._id === card._id && unit.foil === foil) {
            result = result - unit.num;
          }
        }
      );

      if (result < 0) {
        console.log(`Card ${JSON.stringify(card)} has negative number ${result}`);
        result = 0;
      }

      return result;
    }
  };

  mergeWithChanges = (original, changes) => {
    if (!changes.length) {
      return original?.length ? original : [];
    }

    let _original = JSON.parse(JSON.stringify(original));

    changes.forEach(
      change => {
        let found = _original.some(
          unit => {
            if (unit.card._id === change.card._id && unit.foil === change.foil) {
              unit.num += change.num;
              return true;
            }
            return false;
          }
        );

        if (!found) {
          _original.push(change);
        }
      }
    );

    return _original;
  }

  triggerNotification = ({ variant, message }) => {
    this.setState({
      openSnackbar: true,
      snackbarProps: {
        variant,
        message,
      },
    });
  };

  handleSnackbarClose = () => {
    this.setState({ openSnackbar: false });
  };

  calculateStats = (deck) => {
    return;
  };

  handleChangeRefineTab = (e, newValue) => {
    this.setState({ refineTabIndex: newValue });
  };

  handleChangeUtilityTab = (e, newValue) => {
    this.setState({ utilityTabIndex: newValue });
  };

  handleChangeListTab = (e, newValue) => {
    this.setState({ listTabIndex: newValue});
  };

  render() {
    const { imaginary, classes, cards, collection, deck, changes } = this.props;
    const { keyword, searchResults, isSubmitting, refineTabIndex, utilityTabIndex, listTabIndex } = this.state;

    let tempMainDeck = [];
    let tempSideDeck = [];
    let tempMainChanges = [];
    let tempSideChanges = [];

    if (deck?.cards?.length) {
      deck.cards.forEach(
        unit => {
          if (unit.isMain) {
            tempMainDeck.push(unit);
          } else {
            tempSideDeck.push(unit);
          }
        }
      );
    }

    extremelySortCards(tempMainDeck, ['cmc', 'color', 'name']);
    extremelySortCards(tempSideDeck, ['cmc', 'color', 'name']);

    if (changes?.length) {
      changes.forEach(
        unit => {
          if (unit.isMain) {
            tempMainChanges.push(unit);
          } else {
            tempSideChanges.push(unit);
          }
        }
      );
    }

    tempMainDeck = this.mergeWithChanges(tempMainDeck, tempMainChanges);
    tempSideDeck = this.mergeWithChanges(tempSideDeck, tempSideChanges);

    if (!deck) {
      return <LoadingSpinner />;
    }

    if (imaginary && !cards.length) {
      return <LoadingSpinner />;
    }

    if (!imaginary && !collection) {
      return <LoadingSpinner />;
    }

    return (
      <ThemeProvider theme={theme}>
        {isSubmitting &&
          <LinearProgress id="progress__submitting" />
        }

        <Container
          className={classes.container}
          maxWidth="xl"
          aria-describedby="progress__submitting"
          aria-busy={isSubmitting}
        >
          <Grid
            className={classes.gridContainer}
            container
            spacing={1}
          >
            <Grid
              item
              xs={12}
              lg={9}
              md={8}
            >
              <Card className={classes.cardRefine}>
                <div>
                  <Tabs
                    className={classes.tabContainer}
                    value={refineTabIndex}
                    onChange={this.handleChangeRefineTab}
                    variant="standard"
                    category="refineTab"
                    aria-label="tabs refineTab"
                    textColor="primary"
                    indicatorColor="primary"
                  >
                    <Tab
                      className={classes.tabItem}
                      icon={<SearchIcon />}
                      aria-label="search"
                      {...a11yProps('refineTab', 0)}
                    />
                    <Tab
                      className={classes.tabItem}
                      icon={<FilterListIcon />}
                      aria-label="filter"
                      {...a11yProps('refineTab', 1)}
                    />
                  </Tabs>
                  <TabPanel
                    className={classes.tabPanel}
                    value={refineTabIndex}
                    index={SEARCH}
                  >
                    <form onSubmit={this.onSearchSubmit}>
                      <FormControl className={classes.formControl}>
                        <InputLabel htmlFor="search__input">
                          Card name
                        </InputLabel>
                        <Input
                          id="search__input"
                          aria-describedby="search__input__helper"
                          type="text"
                          value={keyword}
                          onChange={this.onSearchChange}
                        />
                        <FormHelperText id="search__input__helper">
                          Enter the card name
                        </FormHelperText>
                      </FormControl>
                    </form>
                  </TabPanel>
                  <TabPanel
                    className={classes.tabPanel}
                    value={refineTabIndex}
                    index={FILTER}
                  >
                    <FakeContent lines={3}/>
                  </TabPanel>
                </div>
              </Card>

              <Paper className={classes.utilityPaper}>
                <Tabs
                  className={classes.tabContainer}
                  value={this.state.utilityTabIndex}
                  onChange={this.handleChangeUtilityTab}
                  variant="standard"
                  category="utilityTab"
                  aria-label="tabs utilityTab"
                  textColor="primary"
                  indicatorColor="primary"
                >
                  <Tab
                    className={classes.tabItem}
                    icon={<SearchIcon />}
                    aria-label="result"
                    {...a11yProps('utilityTab', 0)}
                  />
                  <Tab
                    className={classes.tabItem}
                    icon={<FilterIcon />}
                    aria-label="view"
                    {...a11yProps('utilityTab', 1)}
                  />
                </Tabs>
                <TabPanel
                  className={classes.tabPanel}
                  value={utilityTabIndex}
                  index={RESULT}
                >
                  {!searchResults.length &&
                    <FakeContent lines={3} />
                  }

                  {!!searchResults.length &&
                    <React.Fragment>
                      {searchResults.map(
                        ({ card, foil, num }) => (
                          <CompactCardItem
                            key={card._id}
                            card={card}
                            num={this.calculateRemainings({ card, foil, num })}
                            onAddPressed={this.onAddToChanges}
                            onSubstractPressed={this.onSubstractFromChanges}
                          />
                        )
                      )}
                    </React.Fragment>
                  }
                </TabPanel>
                <TabPanel
                  className={classes.tabPanel}
                  value={utilityTabIndex}
                  index={VIEW}
                >
                  {(listTabIndex === MAIN && tempMainDeck.length) &&
                    <CardLayout
                      size="normal"
                      cards={tempMainDeck}
                    />
                  }
                  {(listTabIndex === SIDE && tempSideDeck.length) &&
                    <CardLayout
                      size="normal"
                      cards={tempSideDeck}
                    />
                  }
                </TabPanel>
              </Paper>
            </Grid>

            <Grid
              item
              xs={12}
              lg={3}
              md={4}
            >
              {deck
                ?
                <Card className={classes.paperDeckSummary}>
                  <div className={classes.paperDeckImage}>
                    <img
                      className={classes.innerImage}
                      src={deck.avatar}
                      alt={deck.name}
                    />
                  </div>
                  <div className={classes.paperDeckContent}>
                    <Typography
                      className={classes.paperDeckName}
                      component="h6"
                      variant="h6"
                    >
                      {deck.name}
                    </Typography>
                    <div className={classes.paperDeckColors}>
                      {deck.colors &&
                        deck.colors.map(
                          (color, i) => <CardSymbol key={i} type={color} size="small" />
                        )
                      }
                    </div>

                    <div className={classes.paperDeckButtons}>
                      <div>
                        <Button
                          className={classes.paperDeckButton}
                          variant="contained"
                          type="button"
                          color="primary"
                          onClick={this.onSubmitChanges}
                          disabled={isSubmitting || !changes?.length}
                        >
                          Submit
                        </Button>
                      </div>
                      &nbsp;
                      <div>
                        <Button
                          className={classes.paperDeckButton}
                          variant="contained"
                          type="button"
                          color="secondary"
                          onClick={this.onClearChanges}
                          disabled={isSubmitting || !changes?.length}
                        >
                          Clear
                        </Button>
                      </div>
                    </div>
                  </div>
                </Card>
                :
                <LoadingSpinner />
              }

              <Paper className={classes.paperCardList}>
                <Tabs
                  className={classes.tabContainer}
                  value={listTabIndex}
                  onChange={this.handleChangeListTab}
                  variant="standard"
                  category="listTab"
                  aria-label="tabs listTab"
                  textColor="primary"
                  indicatorColor="primary"
                >
                  <Tab
                    className={classes.tabItem}
                    aria-label="main"
                    label="Main"
                    {...a11yProps('listTab', 0)}
                  />
                  <Tab
                    className={classes.tabItem}
                    aria-label="side"
                    label="Side"
                    {...a11yProps('listTab', 1)}
                  />
                </Tabs>
                <TabPanel
                  className={classes.tabPanel}
                  value={listTabIndex}
                  index={MAIN}
                >
                  {tempMainDeck.length
                    ?
                    <React.Fragment>
                      {
                        tempMainDeck.map(
                          ({ card, foil, num}) => (
                            num > 0 &&
                            <CardTitle
                              key={card._id}
                              card={card}
                              foil={foil}
                              num={num}
                              onSelfClick={this.onSubstractFromChanges}
                            />
                          )
                        )
                      }
                    </React.Fragment>
                    :
                    <FakeContent />
                  }
                </TabPanel>
                <TabPanel
                  className={classes.tabPanel}
                  value={listTabIndex}
                  index={SIDE}
                >
                  {tempSideDeck.length
                    ?
                    <React.Fragment>
                      {
                        tempSideDeck.map(
                          ({ card, foil, num}) => (
                            num > 0 &&
                            <CardTitle
                              key={card._id}
                              card={card}
                              foil={foil}
                              num={num}
                              onSelfClick={this.onSubstractFromChanges}
                            />
                          )
                        )
                      }
                    </React.Fragment>
                    :
                    <FakeContent />
                  }
                </TabPanel>
              </Paper>
            </Grid>
          </Grid>
        </Container>
      </ThemeProvider>
    );
  }
}

const StyledDeckEditView = withStyles(styles)(DeckEditView);

const mapStateToProps = state => ({
  cards: state.cards,
  collection: state.collection,
  deck: state.deck.currentDeck || { },
  changes: state.changes,
});

export default connect(mapStateToProps, {
  fetchCards,
  fetchCollection,
  fetchDeck,
  submitDeckChanges,
  editChanges,
  clearChanges,
})(StyledDeckEditView);