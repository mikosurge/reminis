import React from 'react';
import { connect } from 'react-redux';

import { withStyles } from '@material-ui/core/styles';
import {
  Fab,
  Grid,
} from '@material-ui/core';
import SearchIcon from '@material-ui/icons/Search';

import CardLayout from '../card/CardLayout';

import { fetchCollection } from '../../../actions';
import LoadingSpinner from '../../widget/Loading';

import { extremelySortCards, SORT_TYPES } from '../../../shared/sorting';
const { COLOR, PRICE, NAME, EXP, INDEX, TIME } = SORT_TYPES;


const styles = theme => ({
  container: {
    paddingLeft: theme.spacing(2),
    paddingTop: theme.spacing(1),
  },
  collectionFab: {
    position: 'fixed',
    bottom: theme.spacing(2),
    right: theme.spacing(2),
    zIndex: '20',
  },
});


class CollectionView extends React.Component {
  state = {
    currentSize: 'normal',
  };

  componentDidMount() {
    this.props.fetchCollection();
  }

  changeSize = () => {
    let nextSize = this.state.currentSize === 'normal' ? 'large' : 'normal';
    this.setState({ currentSize: nextSize });
  };

  render() {
    const { classes, collection } = this.props;

    if (!collection?.cards?.length) {
      return <LoadingSpinner />
    }

    let sortedCards = extremelySortCards(collection.cards, [PRICE, NAME, COLOR], false);
    // let sortedCards = extremelySortCards(collection.cards, [TIME, EXP, INDEX]);

    return (
      <React.Fragment>
        <Grid
          className={classes.container}
          container
        >
          <CardLayout
            size={this.state.currentSize}
            cards={sortedCards}
          />
        </Grid>
        <Fab
          className={classes.collectionFab}
          aria-label="switch"
          color="primary"
          onClick={this.changeSize}
        >
          <SearchIcon />
        </Fab>
      </React.Fragment>
    );
  }
}

const StyledCollectionView = withStyles(styles)(CollectionView);

const mapStateToProps = state => ({
  collection: state.collection,
});

export default connect(mapStateToProps, {
  fetchCollection,
})(StyledCollectionView);