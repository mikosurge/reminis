import React from 'react';
import { connect } from 'react-redux';

import { withStyles } from '@material-ui/core/styles';
import {
  Button,
  Card,
  Container,
  FormControl,
  FormHelperText,
  Grid,
  Input,
  InputLabel,
  Paper,
  Typography,
} from '@material-ui/core';

import CardTitle from '../card/CardTitle';
import CompactCardItem from '../card/CompactCardItem';
import FakeContent from '../../widget/FakeContent';
import LoadingSpinner from '../../widget/Loading';
import {
  fetchCards,
  fetchCollection,
  editChanges,
  clearChanges,
  submitCollectionChanges,
} from '../../../actions';


const styles = theme => ({
  container: {
    padding: `0 ${theme.spacing(1)}px`,
  },
  gridContainer: {

  },
  cardSearch: {
    margin: `${theme.spacing(1)}px 0`,
    padding: `${theme.spacing(2)}px ${theme.spacing(4)}px`,
  },
  formControl: {
    width: '100%',
  },
  paperResult: {
    margin: `${theme.spacing(1)}px 0`,
    padding: theme.spacing(1),
  },
  paperDeck: {
    margin: `${theme.spacing(1)}px 0`,
    padding: theme.spacing(1),
  },
  paperDeckButtons: {
    display: 'flex',
    flexDirection: 'row',
    justifyContent: 'flex-end',
  },
  paperDeckButton: {
    fontSize: theme.spacing(1.5),
    padding: `0 ${theme.spacing(1)}px`,
  },
  paperDeckSummary: {
    margin: `${theme.spacing(1)}px 0`,
    padding: theme.spacing(1),
    display: 'flex',
    flexDirection: 'row',
    border: '1px solid #AAA',
  },
  paperDeckImage: {
    width: '50%',
    maxHeight: theme.spacing(16),
    padding: theme.spacing(1),
  },
  paperDeckContent: {
    width: '50%',
    textAlign: 'right',
    padding: theme.spacing(1),
    display: 'flex',
    flexDirection: 'column',
    justifyContent: 'space-evenly',
  },
  paperDeckName: {
    lineHeight: 1.2,
  },
  paperDeckColors: {

  },
  paperCardList: {
    padding: theme.spacing(1),
  },
  innerImage: {
    maxWidth: '100%',
    maxHeight: '100%',
  },
  resultPaper: {
    padding: theme.spacing(1),
  },
});

class CollectionEditView extends React.Component {
  state = {
    keyword: '',
    searchResults: [],
  };

  componentDidMount() {
    const { fetchCollection, fetchCards, clearChanges } = this.props;
    clearChanges();
    fetchCards()
      .catch(
        error => console.log(error)
      );
    fetchCollection()
      .catch(
        error => console.log(error)
      );
  }


  /**
   * @description Search in all cards
   * @return {Array} The list of cards that match the search pattern
   */
  performSearch = (kw, isSubmit) => {
    kw = kw.toLowerCase();
    if (kw.length < 5 && !isSubmit) {
      this.setState({
        searchResults: [],
      });
    } else {
      let tempResults = [];
      const { cards, collection } = this.props;
      cards.forEach(
        card => {
          if (card.name.toLowerCase().includes(kw)) {
            tempResults.push({ card, num: { foil: 0, normal: 0 } });
          }
        }
      );

      tempResults = this.addNumber(tempResults, collection.cards);

      this.setState({
        searchResults: tempResults,
      });
    }
  };


  onSearchSubmit = e => {
    e.preventDefault();
    this.performSearch(this.state.keyword, true);
  };

  onSearchChange = ({ target: { value } }) => {
    this.setState({
      keyword: value,
    });

    this.performSearch(value, false);
  };

  onAddToChanges = ({ card, foil }) => {
    const { editChanges } = this.props;
    const { searchResults } = this.state;

    let originalNumber = null;
    let found = searchResults.some(
      unit => {
        if (unit.card._id === card._id) {
          originalNumber = foil ? unit.num.foil : unit.num.normal;
          return true;
        }
        return false;
      }
    );

    if (found) {
      let perform = originalNumber === 0 ? 'insert' : 'update';
      console.log(`Gonna add 1 ${card.name}, foil=${foil}, perform=${perform}`);
      editChanges({
        card,
        foil,
        num: 1,
        perform,
      });
    } else {
      console.log(`Card not found: ${JSON.stringify(card)}`);
    }
  };

  onSubstractFromChanges = ({ card, foil }) => {
    const { editChanges, changes } = this.props;
    const { searchResults } = this.state;

    let originalNumber = null;
    let foundInSearch = searchResults.some(
      unit => {
        if (unit.card._id === card._id) {
          originalNumber = foil ? unit.num.foil : unit.num.normal;
          return true;
        }
        return false;
      }
    );

    let currentNumber = originalNumber;
    let foundInChanges = changes.some(
      unit => {
        if (unit.card._id === card._id && unit.foil === foil) {
          currentNumber += unit.num;
          return true;
        }
        return false;
      }
    );

    console.log(`Current number = ${currentNumber}`);

    if (foundInSearch) {
      let perform = currentNumber === 1 ? 'remove' : originalNumber === 0 ? 'insert' : 'update';
      console.log(`Gonna remove 1 ${card.name}, foil=${foil}, perform=${perform}`);
      editChanges({
        card,
        foil,
        num: -1,
        perform,
      });
    } else {
      console.log(`Card not found: ${JSON.stringify(card)}`);
    }
  }

  onSubmitChanges = e => {
    const { submitCollectionChanges, fetchCollection, clearChanges } = this.props;
    submitCollectionChanges()
      .then(
        () => {
          return fetchCollection();
        }
      )
      .then(
        () => {
          clearChanges();
          this.performSearch(this.state.keyword, true);
        }
      )
      .catch(
        error => console.log(error)
      );
  };

  onClearChanges = e => {
    this.props.clearChanges();
  };

  /**
   * @description Merge all number of foil/non-foil to one list of cards
   * @param {Array<Card>} searchResults - The list of cards returned by searching,
   * has a 'num' object of 'num: { foil: 0, normal: 0 }'
   * @param {Array<{ Card, foil, num }>} collection - The card collection of the user
   * @return {Array<Card>} The list of cards after merging with 'num: { foil: x, normal: x }'
   */
  addNumber = (searchResults, collection) => {
    if (!searchResults) {
      return [];
    }

    if (!collection.length) {
      return searchResults;
    }

    collection.forEach(
      ({ card, foil, num }) => {
        searchResults.some(
          item => {
            if (item.card._id === card._id) {
              if (foil) {
                item.num.foil = num;
              } else {
                item.num.normal = num;
              }
              return true;
            }
            return false;
          }
        )
      }
    );

    return searchResults;
  };

  calculateRemainings = ({ card, num }) => {
    const { changes } = this.props;
    const _num = JSON.parse(JSON.stringify(num));

    changes.forEach(
      item => {
        if (item.card._id === card._id) {
          if (item.foil) {
            _num.foil += item.num;
          } else {
            _num.normal += item.num;
          }
        }
      }
    );

    if (_num.foil < 0 || _num.normal < 0) {
      console.log(`${card.name} has its current number of ${JSON.stringify(_num)}`);
    }

    return _num;
  };

  // This method is all good, but I think calculate current number is ok
  mergeWithChanges = () => {
    const { changes } = this.props;
    const { searchResults } = this.state;

    if (!searchResults.length) {
      return [];
    }

    if (!changes.length) {
      return searchResults?.length ? searchResults : [];
    }

    // In case no unit in search results has a change, it will be pristine
    let changedSearchResults = JSON.parse(JSON.stringify(searchResults));
    changes.forEach(
      change => {
        changedSearchResults.some(
          unit => {
            if (unit.card._id === change.card._id) {
              if (change.foil) {
                unit.num.foil += change.num;
              } else {
                unit.num.normal += change.normal;
              }
              return true;
            }
            return false;
          }
        );
      }
    );

    return changedSearchResults;
  };

  render() {
    const {
      auth: { user: { realName } },
      classes,
      cards,
      collection,
      changes,
    } = this.props;

    const { keyword, searchResults } = this.state;

    if (!collection || !cards.length) {
      return <LoadingSpinner />
    }

    return (
      <Container className={classes.container} maxWidth='xl'>
        <Grid
          className={classes.gridContainer}
          container
          spacing={1}
        >
          <Grid
            item
            xs={12}
            lg={9}
            md={8}
          >
            <Card className={classes.cardSearch}>
              <form onSubmit={this.onSearchSubmit}>
                <FormControl className={classes.formControl}>
                  <InputLabel htmlFor="search__input">
                    Card name
                  </InputLabel>
                  <Input
                    id="search__input"
                    aria-describedby="search__input__helper"
                    type="text"
                    value={keyword}
                    onChange={this.onSearchChange}
                  />
                  <FormHelperText id="search__input__helper">
                    Enter the card name
                  </FormHelperText>
                </FormControl>
              </form>
            </Card>

            {!searchResults.length &&
              <FakeContent wrapper={Paper} />
            }

            {!!searchResults.length &&
              <Paper className={classes.resultPaper}>
                {searchResults.map(
                  ({ card, num }) => (
                    <CompactCardItem
                      key={card._id}
                      card={card}
                      num={this.calculateRemainings({ card, num })}
                      onAddPressed={this.onAddToChanges}
                      onSubstractPressed={this.onSubstractFromChanges}
                    />
                  )
                )}
              </Paper>
            }
          </Grid>

          <Grid
            item
            xs={12}
            lg={3}
            md={4}
          >
            {
              collection
              ?
              <Card className={classes.paperDeckSummary}>
                <div className={classes.paperDeckImage}>
                  <img
                    className={classes.innerImage}
                    src="/defaultart.jpg"
                    alt={`${realName}'s Collection`}
                  />
                </div>
                <div className={classes.paperDeckContent}>
                  <Typography
                    className={classes.paperDeckName}
                    component="h6"
                    variant="h6"
                  >
                    {`${realName}'s Collection`}
                  </Typography>

                  <div className={classes.paperDeckButtons}>
                    <div>
                      <Button
                        className={classes.paperDeckButton}
                        variant="contained"
                        type="button"
                        color="primary"
                        onClick={this.onSubmitChanges}
                      >
                        Submit
                      </Button>
                    </div>
                    &nbsp;
                    <div>
                      <Button
                        className={classes.paperDeckButton}
                        variant="contained"
                        type="button"
                        color="secondary"
                        onClick={this.onClearChanges}
                      >
                        Clear
                      </Button>
                    </div>
                  </div>
                </div>
              </Card>
              :
              <LoadingSpinner />
            }

            {changes?.length
              ?
              <Paper className={classes.paperCardList}>
                {
                  changes.map(
                    ({ card, foil, num }) => (
                      <CardTitle
                        key={card._id}
                        card={card}
                        foil={foil}
                        num={num}
                        onSelfClick={this.onSubstractFromChanges}
                      />
                    )
                  )
                }
              </Paper>
              :
              <FakeContent wrapper={Paper} />
            }
          </Grid>
        </Grid>
      </Container>
    );
  }
}

const StyledCollectionEditView = withStyles(styles)(CollectionEditView);

const mapStateToProps = state => ({
  auth: state.auth,
  cards: state.cards,
  collection: state.collection,
  changes: state.changes,
});

export default connect(mapStateToProps, {
  fetchCards,
  fetchCollection,
  submitCollectionChanges,
  editChanges,
  clearChanges,
})(StyledCollectionEditView);