import React from 'react';

import { makeStyles } from '@material-ui/core/styles';
import {
  Button,
  Card
} from '@material-ui/core';

import AddIcon from '@material-ui/icons/Add';
import RemoveIcon from '@material-ui/icons/Remove';

import CardSymbol from '../common/CardSymbol';
import CardTitle from './CardTitle';
import { getEmblemURL, getSymbolList } from '../../../shared/utils';


const useStyles = makeStyles(theme => ({
  container: {
    display: 'flex',
    flexDirection: 'row',
    alignItems: 'stretch',
    fontFamily: 'Beleren',
    minHeight: theme.spacing(10),
    margin: `${theme.spacing(1)}px 0`,
    padding: `${theme.spacing(1)}px 0`,
    border: `1px solid #AAA`,
  },
  cardSetAndIndex: {
    width: '10%',
    display: 'flex',
    flexDirection: 'column',
    justifyContent: 'center',
    textAlign: 'center',
    margin: `0 ${theme.spacing(1)}px`,
  },
  cardEmblemContainer: {
    display: 'flex',
    alignItems: 'center',
    alignContent: 'center',
  },
  cardEmblem: {
    margin: 'auto',
    maxHeight: theme.spacing(4),
  },
  cardIndex: {
    cursor: 'pointer',
  },
  cardCosts: {
    width: '20%',
    display: 'flex',
    flexDirection: 'column',
    justifyContent: 'center',
    textAlign: 'center',
    margin: `0 ${theme.spacing(1)}px`,
    fontSize: '1rem',
  },
  cardManaCost: {

  },
  cardPrice: {

  },
  cardContent: {
    width: '50%',
    display: 'flex',
    flexDirection: 'column',
    justifyContent: 'center',
    textAlign: 'center',
    margin: `0 ${theme.spacing(1)}px`,
  },
  cardTitle: {

  },
  cardType: {

  },
  cardGap: {
    width: '10%',
  },
  cardPossess: {
    width: '10%',
    display: 'flex',
    flexFlow: 'column',
    justifyContent: 'center',
    textAlign: 'center',
    margin: `0 ${theme.spacing(1)}px`,
  },
  cardButtonContainer: {
    flexGrow: 1,
    margin: `${theme.spacing(0.5)}px 0`,
  },
  cardNumber: {
    fontSize: '1.2rem',
  },
  cardAdjustButtons: {
    flexGrow: 1,
    margin: `${theme.spacing(0.5)}px 0`,
    display: 'flex',
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'stretch',
  },
  cardAdjustButton: {
    height: '100%',
    minWidth: '40%',
    flexGrow: 1,
    '&:first-child': {
      marginRight: '1px',
    },
    '&:last-child': {
      marginLeft: '1px',
    },
  },
  cardAdjustButtonIcon: {
    fontSize: theme.spacing(2),
  },
}));

const getMarketPrice = (prices, isFoil) => {
  const accessor = isFoil ? 'foil' : 'normal';
  if (!prices?.[accessor]) {
    return '?';
  } else {
    return `$${prices[accessor].market || '?'}`;
  }
};

export default function CompactCardItem(props) {
  const classes = useStyles();
  const {
    card,
    num,
    onAddPressed,
    onSubstractPressed,
  } = props;

  const {
    set,
    collectorNumber,
    manaCost,
    rarity,
    typeLine,
    tcgPrices,
  } = card;

  const iconizeSet = (set, rarity, size) => {
    return (
      <img
        className={classes.cardEmblem}
        src={getEmblemURL(set, rarity, size)}
        alt={set}
      />
    );
  };

  const iconizeMana = (manaCost, size) => {
    let icons = getSymbolList(manaCost).map(
      (symbol, i) => <CardSymbol key={i} type={symbol} size={size} />
    )
    return <> {icons} </>;
  };

  const onAddClick = item => e => {
    onAddPressed(item);
  };

  const onSubstractClick = item => e => {
    if (typeof num !== 'object') {
      onSubstractPressed(item);
      return;
    }

    let currentNumber = isFoil ? num.foil : num.normal;
    if (currentNumber > 0) {
      onSubstractPressed(item);
    }
  };

  const onSwitchFoil = () => {
    switchFoil(!isFoil);
    setCardPrice(getMarketPrice(tcgPrices, !isFoil));
  }

  let [isFoil, switchFoil] = React.useState(false);
  let [cardPrice, setCardPrice] = React.useState(getMarketPrice(tcgPrices, false));

  return (
    <Card className={classes.container}>
      <div className={classes.cardSetAndIndex}>
        <div className={classes.cardEmblemContainer}>
          {iconizeSet(set, rarity, 'medium')}
        </div>
        <div
          className={classes.cardIndex}
          onClick={onSwitchFoil}
        >
          {collectorNumber}
        </div>
      </div>

      <div className={classes.cardCosts}>
        <div className={classes.cardManaCost}>
          {iconizeMana(manaCost, 'small')}
        </div>
        <div className={classes.cardPrice}>
          {cardPrice}
        </div>
      </div>

      <div className={classes.cardContent}>
        <div className={classes.cardTitle}>
          <CardTitle
            card={card}
            foil={isFoil}
            num={isFoil ? num.foil : num.normal}
            isSimple={true}
          />
        </div>
        <div className={classes.cardType}>
          {typeLine}
        </div>
      </div>

      <div className={classes.cardGap}>

      </div>

      <div className={classes.cardPossess}>
        <div className={classes.cardAdjustButtons}>
          <Button
            className={classes.cardAdjustButton}
            variant="contained"
            color="primary"
            size="small"
            onClick={onAddClick({ card, foil: isFoil })}
          >
            <AddIcon className={classes.cardAdjustButtonIcon} />
          </Button>
          <Button
            className={classes.cardAdjustButton}
            variant="contained"
            color="secondary"
            size="small"
            onClick={onSubstractClick({ card, foil: isFoil})}
          >
            <RemoveIcon className={classes.cardAdjustButtonIcon} />
          </Button>
        </div>
        <div className={classes.cardNumber}>
          {typeof num === 'object'
            ?
            isFoil ? num.foil : num.normal
            :
            num
          }
        </div>
      </div>
    </Card>
  );
}