import React from 'react';
import clsx from 'clsx';

import { withStyles } from '@material-ui/core/styles';
import { getEmblemURL, getSymbolList } from '../../../shared/utils';

import CardSymbol from '../common/CardSymbol';


const styles = theme => ({
  container: {
    display: 'flex',
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    margin: `${theme.spacing(0.5)}px 0`,
  },
  outer: {
    flexGrow: 1,
    borderStyle: 'solid',
    borderWidth: '2px',
    borderRadius: '500px',
    cursor: 'pointer',
  },
  outerC: {
    borderColor: '#BABEC0',
  },
  outerW: {
    borderColor: '#F6F8F1',
  },
  outerU: {
    borderColor: '#0077B2',
  },
  outerB: {
    borderColor: '#221F16',
  },
  outerR: {
    borderColor: '#E44B2A',
  },
  outerG: {
    borderColor: '#007336',
  },
  outerM: {
    borderColor: '#D8BC33',
  },
  inner: {
    border: '2px solid #777',
    margin: 0,
    padding: `0 ${theme.spacing(1)}px`,
    borderRadius: '500px',
  },
  innerC: {
    background: '#DDDDDD',
  },
  innerW: {
    background: '#F9FAF7',
  },
  innerU: {
    background: '#C4DFEC',
  },
  innerB: {
    background: '#CBC4C4',
  },
  innerR: {
    background: '#F5D5C5',
  },
  innerG: {
    background: '#CBDBD4',
  },
  innerM: {
    background: '#E5D8AA',
  },
  content: {
    display: 'flex',
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    minHeight: theme.spacing(3),
  },
  simpleContent: {
    justifyContent: 'center',
  },
  emblem: {
    display: 'flex',
    alignItems: 'center',
    maxHeight: theme.spacing(3),
  },
  name: {
    fontFamily: 'Beleren',
    fontSize: theme.spacing(2),
    cursor: 'pointer',
    padding: `0 ${theme.spacing(1)}`,
  },
  manaCost: {
    display: 'flex',
    alignItems: 'center',
    height: '100%',
  },
  possess: {
    marginLeft: theme.spacing(1),
    fontSize: '1.2rem',
    fontFamily: 'Beleren',
  }
});


const basicColors = ['W', 'U', 'B', 'R', 'G'];


class CardTitle extends React.Component {
  determineColor = (clazz, colorArray) => {
    const { classes } = this.props;
    let suffix = null;

    if (colorArray.length === 0) {
      suffix = 'C';
    } else if (colorArray.length > 1) {
      suffix = 'M';
    } else {
      if (basicColors.includes(colorArray[0])) {
        suffix = colorArray[0];
      } else {
        suffix = 'C';
      }
    }

    return classes[`${clazz}${suffix}`];
  };

  iconizeSet = (set, rarity) => {
    return (
      <img
        src={getEmblemURL(set, rarity)}
        alt={set}
      />
    );
  };

  iconizeMana = (manaCost) => {
    let icons = getSymbolList(manaCost).map(
      (symbol, i) => <CardSymbol key={i} type={symbol} size="small" />
    );

    return <> {icons} </>;
  };

  handleSelfClick = unit => e => {
    if (typeof this.props.onSelfClick === 'function') {
      this.props.onSelfClick(unit);
    }
  };

  render() {
    const {
      classes,
      card,
      foil,
      num,
      isSimple,
    } = this.props;

    const {
      set,
      colorIdentity,
      name,
      rarity,
      manaCost,
    } = card;

    let suffix = foil ? " (Foil)" : "";

    return (
      <div className={classes.container}>
        <div
          className={clsx(classes.outer, this.determineColor('outer', colorIdentity))}
          onClick={this.handleSelfClick({ card, foil })}
        >
          <div className={clsx(classes.inner, this.determineColor('inner', colorIdentity))}>
            <div className={clsx(classes.content, {[classes.simpleContent]: isSimple})}>
              {!isSimple &&
                <div className={classes.emblem}>
                  {this.iconizeSet(set, rarity)}
                </div>
              }
              <div className={classes.name}>
                {`${name}${suffix}`}
              </div>
              {!isSimple &&
                <div className={classes.manaCost}>
                  {this.iconizeMana(manaCost)}
                </div>
              }
            </div>
          </div>
        </div>
        {!isSimple &&
          <div className={classes.possess}>
            {`x${num || 0}`}
          </div>
        }
      </div>
    );
  };
}

export default withStyles(styles)(CardTitle);