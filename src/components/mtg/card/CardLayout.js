import React from 'react';
import { makeStyles } from '@material-ui/core/styles';

import VisualCardItem from './VisualCardItem';


const useStyles = makeStyles(theme => ({
  layout: {
    display: 'flex',
    flexDirection: 'row',
    flexWrap: 'wrap',
  }
}));

/**
 * @description Given the card object, return a URL of a specific card art
 * @param {Card} card - The card object
 * @param {Size} option - The specific card art, one of 'small', 'normal', 'large', 'png', 'artCrop', 'borderCrop'
 * @param {Number} faceIndex - The card side, can be 0 (front) or 1 (back)
 * @return {String} A URL to the specific art, null if not found
 */
const getImage = (card, option, faceIndex = 0) => {
  if (card?.imageUris?.[option]) {
    return card.imageUris[option];
  } else if (card?.cardFaces?.length) {
    return card.cardFaces?.[faceIndex]?.imageUris?.[option];
  } else {
    console.log(`Can not get image URL of card ${JSON.stringify(card)}`);
    return null;
  }
};


export default function CardLayout(props) {
  const { size, cards } = props;
  const classes = useStyles();

  return (
    <div className={classes.layout}>
      {cards.map(
        ({ card, num, foil }, i) => (
          <VisualCardItem
            key={`${card._id}-${i}`}
            num={num}
            src={getImage(card, size)}
            alt={card.name}
            foil={foil}
            {...props}
          />
        )
      )}
    </div>
  );
};