import React from 'react';

import { makeStyles } from '@material-ui/core/styles';

import { CARD_SIZES } from '../../../shared/given';


const useStyles = makeStyles(theme => ({
  container: {
    paddingBottom: 0,
    margin: theme.spacing(0.75),
    position: 'relative',
    overflow: 'hidden',
  },
  card: {
    borderRadius: '4.75% 4%',
    width: '100%',
  },
  normal: {
    borderRadius: '4.75% 4%',
    position: 'absolute',
  },
  foil: {
    borderRadius: '4.75% 4%',
    position: 'absolute',
    '&:after': {
      content: '""',
      color: 'transparent',
      display: 'inline-block',
      position: 'absolute',
      left: 0,
      top: 0,
      opacity: 0.8,
      width: '100%',
      height: '100%',
      borderRadius: '4.75% 4%',
      // background: 'rgba(222,197,37,0)',
      // background: '-moz-linear-gradient(-45deg, rgba(222,197,37,0) 0%, rgba(222,197,37,0) 1%, rgba(255,8,8,0.06) 19%, rgba(239,111,23,0.1) 32%, rgba(222,213,37,0.23) 45%, rgba(36,222,101,0.39) 62%, rgba(47,221,109,0.4) 63%, rgba(216,208,239,0.39) 79%, rgba(131,123,173,0.38) 88%, rgba(136,129,178,0.38) 89%, rgba(193,191,234,0) 100%)',
      // background: '-webkit-gradient(left top, right bottom, color-stop(0%, rgba(222,197,37,0)), color-stop(1%, rgba(222,197,37,0)), color-stop(19%, rgba(255,8,8,0.06)), color-stop(32%, rgba(239,111,23,0.1)), color-stop(45%, rgba(222,213,37,0.23)), color-stop(62%, rgba(36,222,101,0.39)), color-stop(63%, rgba(47,221,109,0.4)), color-stop(79%, rgba(216,208,239,0.39)), color-stop(88%, rgba(131,123,173,0.38)), color-stop(89%, rgba(136,129,178,0.38)), color-stop(100%, rgba(193,191,234,0)))',
      // background: '-webkit-linear-gradient(-45deg, rgba(222,197,37,0) 0%, rgba(222,197,37,0) 1%, rgba(255,8,8,0.06) 19%, rgba(239,111,23,0.1) 32%, rgba(222,213,37,0.23) 45%, rgba(36,222,101,0.39) 62%, rgba(47,221,109,0.4) 63%, rgba(216,208,239,0.39) 79%, rgba(131,123,173,0.38) 88%, rgba(136,129,178,0.38) 89%, rgba(193,191,234,0) 100%)',
      // background: '-o-linear-gradient(-45deg, rgba(222,197,37,0) 0%, rgba(222,197,37,0) 1%, rgba(255,8,8,0.06) 19%, rgba(239,111,23,0.1) 32%, rgba(222,213,37,0.23) 45%, rgba(36,222,101,0.39) 62%, rgba(47,221,109,0.4) 63%, rgba(216,208,239,0.39) 79%, rgba(131,123,173,0.38) 88%, rgba(136,129,178,0.38) 89%, rgba(193,191,234,0) 100%)',
      // background: '-ms-linear-gradient(-45deg, rgba(222,197,37,0) 0%, rgba(222,197,37,0) 1%, rgba(255,8,8,0.06) 19%, rgba(239,111,23,0.1) 32%, rgba(222,213,37,0.23) 45%, rgba(36,222,101,0.39) 62%, rgba(47,221,109,0.4) 63%, rgba(216,208,239,0.39) 79%, rgba(131,123,173,0.38) 88%, rgba(136,129,178,0.38) 89%, rgba(193,191,234,0) 100%)',
      background: 'linear-gradient(135deg, rgba(222,197,37,0) 0%, rgba(222,197,37,0) 1%, rgba(255,8,8,0.06) 19%, rgba(239,111,23,0.1) 32%, rgba(222,213,37,0.23) 45%, rgba(36,222,101,0.39) 62%, rgba(47,221,109,0.4) 63%, rgba(216,208,239,0.39) 79%, rgba(131,123,173,0.38) 88%, rgba(136,129,178,0.38) 89%, rgba(193,191,234,0) 100%)',
    }
  }
}));


/**
 * @description Get the actual height of a stack of several cards.
 * Consider the header height is about 10% of the card's height.
 * The bottommost card is always fully shown, so we need (n - 1) times header height only.
 * @param {Object} size - The card size, contains w (width) and h (height)
 * @param {Number} num - Num of of cards in a stack
 */
const getActualHeight = (size, num) => {
  const headerHeight = Math.ceil(size.h / 10);
  return size.h + (num - 1) * headerHeight;
};


export default function VisualCardItem(props) {
  const classes = useStyles();
  const {
    size, num, src, alt, foil,
  } = props;

  let cardSize = { };
  if (size) {
    cardSize = CARD_SIZES[size];
  }

  function renderCards(size, num, src, alt) {
    let cardArray = [];
    const headerHeight = Math.ceil(size.h / 10);

    for (let i = 0; i < num; i++) {
      let style = {
        top: i * headerHeight,
        zIndex: i * 2,
        height: size.h,
      };

      cardArray.push(
        <div
          className={foil ? classes.foil : classes.normal}
          style={style}
          key={i}
        >
          <img
            className={classes.card}
            src={src}
            alt={alt}
          />
        </div>
      );
    }

    return cardArray;
  }

  return (
    <div
      className={classes.container}
      style={{ width: cardSize.w, height: getActualHeight(cardSize, num) }}
    >
      {renderCards(cardSize, num, src, alt)}
    </div>
  );
};