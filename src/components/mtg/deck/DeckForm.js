import React from 'react';


import { Field, reduxForm } from 'redux-form';

import { withStyles } from '@material-ui/core/styles';

import {
  Button,
  Checkbox,
  FormControl,
  FormControlLabel,
  FormGroup,
  FormHelperText,
  FormLabel,
  Input,
  InputLabel,
  MenuItem,
  Select,
} from '@material-ui/core';


const styles = theme => ({
  form: {
    margin: 'auto',
    padding: theme.spacing(1),
    maxWidth: theme.spacing(60),
  },
  formControl: {
    margin: `${theme.spacing(1)}px 0`,
    '&:first-child': {
      margin: 0,
      marginBottom: theme.spacing(1),
    },
    '&:last-child': {
      margin: 0,
      marginTop: theme.spacing(1),
    },
  },
  buttonGroup: {
    textAlign: 'center',
  },
  button: {
    margin: theme.spacing(0.5),
    fontFamily: "Lucida Sans Unicode, Lucida Grande, sans-serif",
  },
});


const playFormats = [
  {
    value: 'standard',
    text: 'Standard',
  },
  {
    value: 'modern',
    text: 'Modern',
  },
  {
    value: 'commander',
    text: 'Commander',
  },
  {
    value: 'constructed',
    text: "Constructed",
  },
  {
    value: 'brawl',
    text: 'Brawl',
  },
  {
    value: 'pioneer',
    text: 'Pioneer',
  },
];


const validate = values => {
  const errors = { };

  if (!values.deckName) {
    errors.deckName = 'Required';
  } else if (values.deckName.length < 3) {
    errors.deckName = 'The deck name is too short!';
  } else if (values.deckName.length > 25) {
    errors.deckName = 'The deck name is too long!';
  }

  if (!values.deckDescription) {
    errors.deckDescription = 'Required';
  }

  if (values.deckAvatar) {
    if (!/^(http|https):\/\/(\w+.[^/])+\/\w+/.test(values.deckAvatar)) {
      errors.deckAvatar = 'Not a valid URL';
    }
  }

  if (!values.deckFormat) {
    errors.deckFormat = 'Required';
  }

  return errors;
};


class DeckForm extends React.Component {
  state = {
    isImaginaryChecked: false,
  };

  onSubmit = formValues => {
    if (formValues.deckImaginary === undefined) {
      formValues.deckImaginary = false;
    }

    if (typeof this.props.onSubmit === 'function') {
      this.props.onSubmit(formValues);
    }
  };

  handleImaginaryCheckbox = () => {
    const nextState = !this.state.isImaginaryChecked;
    this.setState({
      isImaginaryChecked: nextState
    });
  };

  renderTextInput = ({
    id,
    input,
    label,
    meta: {
      touched, error
    },
    type,
    helperText,
    required,
  }) => {
    const { classes } = this.props;
    const inputId = `deck-form__${id}`;

    return (
      <FormControl
        className={classes.formControl}
        error={touched && !!error}
        required={required}
      >
        <InputLabel htmlFor={inputId}>
          {label}
        </InputLabel>
        <Input
          id={inputId}
          type={type}
          aria-describedby={`${inputId}__helper`}
          {...input}
        />
        <FormHelperText
          id={`${inputId}__helper`}
        >
          {
            touched
            ? error || 'Looks good!'
            : helperText
          }
        </FormHelperText>
      </FormControl>
    );
  };

  renderFormatSelect = ({
    input,
    label,
    meta: {
      touched, error,
    },
    helperText,
    required,
  }) => {
    const { classes } = this.props;

    return (
      <FormControl
        className={classes.formControl}
        error={touched && !!error}
        required={required}
      >
        <InputLabel htmlFor="deck-form__play-format">
          {label}
        </InputLabel>
        <Select
          onChange={this.handleChangeFormat}
          inputProps={{
            name: 'play-format',
            id: 'deck-form__play-format',
          }}
          {...input}
        >
          {playFormats.map(
            format => <MenuItem key={format.value} value={format.value}>{format.text}</MenuItem>
          )}
        </Select>
        <FormHelperText
          id="deck-form__play-format__helper"
        >
          {
            touched
            ? error || 'Looks good!'
            : helperText
          }
        </FormHelperText>
      </FormControl>
    );
  };

  renderImaginaryCheckbox = ({
    input,
    label,
    helperText,
    required,
  }) => {
    const { classes } = this.props;

    return (
      <FormControl className={classes.formControl}>
        <FormControlLabel
          control={
            <Checkbox value="imaginary" />
          }
          label={label}
          labelPlacement="end"
          {...input}
        />
        <FormHelperText
          id="deck-form__imaginary__helper"
        >
          {helperText}
        </FormHelperText>
      </FormControl>
    );
  };


  // We need to disable rippling, due to a strange effect that makes the button not aligned correctly
  render() {
    const { handleSubmit, pristine, reset, submitting, invalid, anyTouched, classes } = this.props;
    const { use } = this.props;

    return (
      <form
        className={classes.form}
        onSubmit={handleSubmit(this.onSubmit)}
      >
        {!!use &&
          <FormLabel
            component="legend"
            required
            error={anyTouched && invalid}
          >
            {use === 'create' ? `Create a deck` : `Edit a deck`}
          </FormLabel>
        }

        <FormGroup>
          <Field
            name="deckName"
            component={this.renderTextInput}
            id="name"
            label="Name"
            helperText="Enter the deck name."
            required
          />
          <Field
            name="deckDescription"
            component={this.renderTextInput}
            id="description"
            label="Description"
            helperText="Enter the deck description."
            required
          />
          <Field
            name="deckAvatar"
            component={this.renderTextInput}
            id="avatar"
            label="Avatar"
            helperText="Enter the URL of the deck avatar."
            required
          />
          <Field
            name="deckFormat"
            component={this.renderFormatSelect}
            label="Format"
            helperText="Enter the play format."
            required
          />
          <Field
            name="deckImaginary"
            component={this.renderImaginaryCheckbox}
            label="Imaginary"
            helperText="Check it if the deck is unreal"
            required
          />
          <div className={classes.buttonGroup}>
            <Button
              className={classes.button}
              disableRipple={true}
              variant="contained"
              color="primary"
              type="submit"
              disabled={submitting}
            >
              Submit
            </Button>
            <Button
              className={classes.button}
              disableRipple={true}
              variant="contained"
              color="secondary"
              disabled={pristine || submitting}
              onClick={reset}
            >
              Clear Data
            </Button>
          </div>
        </FormGroup>
      </form>
    );
  }
}

const StyledDeckForm = withStyles(styles)(DeckForm);

export default reduxForm({
  form: 'DeckForm',
  validate: validate,
})(StyledDeckForm);