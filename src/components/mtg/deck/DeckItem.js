import React from 'react';

import { withStyles } from '@material-ui/core/styles';

import {
  Button,
  Card,
  CardActions,
  CardContent,
  Typography,
} from '@material-ui/core';

import CardSymbol from '../common/CardSymbol';
import CustomLink from '../../widget/CustomLink';


const styles = theme => ({
  card: {
    display: 'flex',
    flexDirection: 'column',
  },
  upper: {
    display: 'flex',
    flexDirection: 'row',
  },
  lower: {
    display: 'flex',
    flexDirection: 'row',
  },
  deckImage: {
    padding: `${theme.spacing(1)}px ${theme.spacing(1)}px 0 ${theme.spacing(1.5)}px`,
    height: theme.spacing(16),
  },
  deckContent: {
    flexGrow: 1,
    padding: `${theme.spacing(1)}px ${theme.spacing(1.5)}px 0 ${theme.spacing(1)}px`,
    textAlign: 'right',
  },
  deckColors: {
    padding: theme.spacing(0.5),
    display: 'flex',
    flexWrap: 'wrap',
    float: 'right',
  },
  deckActions: {
    padding: `${theme.spacing(0.5)}px ${theme.spacing(1)}px`,
    display: 'flex',
    alignItems: 'center',
  },
  deckLastEditted: {
    flexGrow: 1,
    fontStyle: 'italic',
  },
  button: {
    fontFamily: "Lucida Sans Unicode, Lucida Grande, sans-serif",
  },
  symbol: {

  },
  innerImage: {
    height: '100%',
  },
});


const months = ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"];

const transformToMonth = month => {
  if (month < 0 || month > months.length) {
    return "Unknown";
  }

  return months[month];
}

const convertFromISOToDateTime = isoDatetime => {
  let date = new Date(isoDatetime);
  let year = date.getFullYear();
  let month = date.getMonth();
  let day = date.getDate();

  return `${transformToMonth(month)} ${day}, ${year}`;
};


class DeckItem extends React.Component {
  render() {
    const { classes, onDelete, onVisit } = this.props;
    const { _id, name, description, colors, avatar, updatedAt, imaginary } = this.props.deck;

    return (
      <div id="deck-item">
        <Card className={classes.card}>
          <div className={classes.upper}>
            <CardContent className={classes.deckImage}>
              <img
                className={classes.innerImage}
                src={avatar}
                alt={name}
              />
            </CardContent>
            <CardContent className={classes.deckContent}>
              <Typography
                component="h5"
                variant="h5"
              >
                {name}
              </Typography>
              <Typography variant="body2">
                {description}
              </Typography>
              <div className={classes.deckColors}>
                {
                  colors.map(
                    (color, i) => (
                      <CardSymbol
                        key={i}
                        type={color}
                        small
                      />
                    )
                  )
                }
              </div>
            </CardContent>
          </div>
          <CardActions className={classes.deckActions}>
            <Typography
              className={classes.deckLastEditted}
              variant="caption"
            >
              {`Last edited: ${convertFromISOToDateTime(updatedAt)}`}
            </Typography>
            <Button
              className={classes.button}
              variant="text"
              color="primary"
              type="button"
              onClick={onVisit}
              to={imaginary ? `/magic/imaginary-deck/${_id}` : `/magic/deck/${_id}`}
              component={CustomLink}
            >
              Edit
            </Button>
            <Button
              className={classes.button}
              variant="text"
              color="secondary"
              type="button"
              onClick={onDelete}
            >
              Delete
            </Button>
          </CardActions>
        </Card>
      </div>
    );
  }
}

export default withStyles(styles)(DeckItem);