import React from 'react';
import clsx from 'clsx';

import { makeStyles } from '@material-ui/core/styles';

import Mana0 from './symbols/Mana0';
import Mana1 from './symbols/Mana1';
import Mana2 from './symbols/Mana2';
import Mana3 from './symbols/Mana3';
import Mana4 from './symbols/Mana4';
import Mana5 from './symbols/Mana5';
import Mana6 from './symbols/Mana6';
import Mana7 from './symbols/Mana7';
import Mana8 from './symbols/Mana8';
import Mana9 from './symbols/Mana9';
import Mana10 from './symbols/Mana10';
import Mana11 from './symbols/Mana11';
import Mana12 from './symbols/Mana12';
import Mana13 from './symbols/Mana13';
import Mana14 from './symbols/Mana14';
import Mana15 from './symbols/Mana15';
import Mana16 from './symbols/Mana16';
import Mana17 from './symbols/Mana17';
import Mana18 from './symbols/Mana18';
import Mana19 from './symbols/Mana19';
import Mana20 from './symbols/Mana20';
import Mana100 from './symbols/Mana100';
import Mana1000000 from './symbols/Mana1000000';
import Mana2B from './symbols/Mana2B';
import Mana2G from './symbols/Mana2G';
import Mana2R from './symbols/Mana2R';
import Mana2U from './symbols/Mana2U';
import Mana2W from './symbols/Mana2W';
import ManaW from './symbols/ManaW';
import ManaU from './symbols/ManaU';
import ManaB from './symbols/ManaB';
import ManaR from './symbols/ManaR';
import ManaG from './symbols/ManaG';
import ManaC from './symbols/ManaC';
import ManaS from './symbols/ManaS';
import ManaX from './symbols/ManaX';
import ManaY from './symbols/ManaY';
import ManaZ from './symbols/ManaZ';
import ManaBP from './symbols/ManaBP';
import ManaGP from './symbols/ManaGP';
import ManaRP from './symbols/ManaRP';
import ManaUP from './symbols/ManaUP';
import ManaWP from './symbols/ManaWP';
import ManaBG from './symbols/ManaBG';
import ManaBR from './symbols/ManaBR';
import ManaGU from './symbols/ManaGU';
import ManaGW from './symbols/ManaGW';
import ManaRG from './symbols/ManaRG';
import ManaRW from './symbols/ManaRW';
import ManaUB from './symbols/ManaUB';
import ManaUR from './symbols/ManaUR';
import ManaWB from './symbols/ManaWB';
import ManaWU from './symbols/ManaWU';
import ManaHalf from './symbols/ManaHalf';
import ManaInfinity from './symbols/ManaInfinity';
import ManaHR from './symbols/ManaHR';
import ManaHW from './symbols/ManaHW';
import ChaosIcon from './symbols/ChaosIcon';
import EnergyIcon from './symbols/EnergyIcon';
import PhyrexianIcon from './symbols/PhyrexianIcon';
import PlaneswalkerIcon from './symbols/PlaneswalkerIcon';
import TapIcon from './symbols/TapIcon';
import UntapIcon from './symbols/UntapIcon';

// This class name is used to auto-margin between 2 Card symbol
const identityClassName = 'card-symbol';

const useStyles = makeStyles(theme => ({
  symbol: {
    width: 'auto',
    fontSize: theme.spacing(10),
    display: 'inline-block',
    borderRadius: theme.spacing(60),
    WebkitBorderRadius: `${theme.spacing(60)}px`,
    boxShadow: '-1px 1px 0 rgba(0,0,0,0.85)',
    WebkitBoxShadow: '-1px 1px 0 rgba(0,0,0,0.85)',
    overflow: 'hidden',
    colorAdjust: 'exact',
    WebkitColorAdjust: 'exact',
    [`& +.${identityClassName}`]: {
      marginLeft: '2px',
    },
  },
  noShadow: {
    WebkitBoxShadow: 'none',
    boxShadow: 'none',
    WebkitBorderRadius: 0,
    borderRadius: 0,
  },
  small: {
    fontSize: theme.spacing(2.5),
  },
  medium: {
    fontSize: theme.spacing(5),
  },
  big: {
    fontSize: theme.spacing(10),
  }
}));

const mapping = {
  '0': Mana0,
  '1': Mana1,
  '2': Mana2,
  '3': Mana3,
  '4': Mana4,
  '5': Mana5,
  '6': Mana6,
  '7': Mana7,
  '8': Mana8,
  '9': Mana9,
  '10': Mana10,
  '11': Mana11,
  '12': Mana12,
  '13': Mana13,
  '14': Mana14,
  '15': Mana15,
  '16': Mana16,
  '17': Mana17,
  '18': Mana18,
  '19': Mana19,
  '20': Mana20,
  '100': Mana100,
  '1000000': Mana1000000,
  '2B': Mana2B,
  '2G': Mana2G,
  '2R': Mana2R,
  '2U': Mana2U,
  '2W': Mana2W,
  'W': ManaW,
  'U': ManaU,
  'B': ManaB,
  'R': ManaR,
  'G': ManaG,
  'C': ManaC,
  'S': ManaS,
  'X': ManaX,
  'Y': ManaY,
  'Z': ManaZ,
  'BP': ManaBP,
  'GP': ManaGP,
  'RP': ManaRP,
  'UP': ManaUP,
  'WP': ManaWP,
  'BG': ManaBG,
  'BR': ManaBR,
  'GU': ManaGU,
  'GW': ManaGW,
  'RG': ManaRG,
  'RW': ManaRW,
  'UB': ManaUB,
  'UR': ManaUR,
  'WB': ManaWB,
  'WU': ManaWU,
  'Half': ManaHalf,  // ½
  'Infinity': ManaInfinity,  // ∞
  'HR': ManaHR,
  'HW': ManaHW,
  'CHAOS': ChaosIcon,
  'E': EnergyIcon,
  'P': PhyrexianIcon,
  'PW': PlaneswalkerIcon,
  'T': TapIcon,
  'Q': UntapIcon,
}

const noShadowIcons = ['HR', 'HW', 'P', 'E', 'CHAOS', 'PW'];

export default function CardSymbol(props) {
  const classes = useStyles();
  const { size, type } = props;

  let symbolSize = classes[size];
  let Symbol = mapping[type] || ManaInfinity;
  let noShadow = noShadowIcons.includes(type);

  return (
    <Symbol
      className={clsx(classes.symbol,
        {
          [classes.noShadow]: noShadow,
        },
        symbolSize,
        identityClassName,
      )}
    />
  );
};