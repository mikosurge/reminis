import React from 'react';

import { withStyles } from '@material-ui/core/styles';

import {
  Button,
  Container,
  Grid,
} from '@material-ui/core';

import { connect } from 'react-redux';

import EnglishWord from '../reader/card/EnglishWord';
import EnglishPhrase from '../reader/card/EnglishPhrase';

import {
  fetchEnglishWords,
  saveEnglishWord,
} from '../../actions';

import {
  severalRandomsInList,
} from '../../shared/utils';


const styles = theme => ({
  container: {
    padding: theme.spacing(0, 1),
  },
  gridContainer: {

  },
});


class GlossaryBoard extends React.Component {
  state = {
    isSubmitting: false,
  };

  render() {
    const { classes, words, phrases, currentWord, currentPhrase } = this.props;
    const { isSubmitting } = this.state;

    let randomWords = severalRandomsInList(words, 4);
    let randomPhrases = severalRandomsInList(phrases, 1);

    return (
      <Container
        className={classes.container}
        maxWidth="xl"
        aria-describedby="progress_submitting"
        aria-busy={isSubmitting}
      >
        <Grid
          className={classes.gridContainer}
          container
          direction="row"
          spacing={2}
        >
          {!!randomPhrases.length ?
            (randomPhrases.map(
              (phrase, i) => (
                <Grid
                  key={i}
                  item
                  sm={12}
                >
                  <EnglishPhrase phrase={phrase} />
                </Grid>
              )
            ))
            :
            null
          }

          {!!randomWords.length ?
            (randomWords.map(
              (word, i) => (
                <Grid
                  key={i}
                  item
                  sm={12}
                  md={6}
                >
                  <EnglishWord word={word} />
                </Grid>
              )
            ))
            :
            null
          }
        </Grid>
      </Container>
    );
  }
};

const StyledGlossaryBoard = withStyles(styles)(GlossaryBoard);

const mapStateToProps = state => ({
  currentWord: state.glossary.currentWord,
  words: state.glossary.words,
  currentPhrase: state.glossary.currentPhrase,
  phrases: state.glossary.phrases,
});

export default connect(mapStateToProps, {
  fetchEnglishWords,
  saveEnglishWord,
})(StyledGlossaryBoard);