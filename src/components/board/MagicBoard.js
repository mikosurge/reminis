import React from 'react';
import { connect } from 'react-redux';

import {
  Avatar,
  Button,
  Card,
  Container,
  Dialog,
  DialogActions,
  DialogContent,
  DialogContentText,
  DialogTitle,
  Divider,
  Grid,
  Hidden,
  Paper,
  Typography,
} from '@material-ui/core';
import { withStyles } from '@material-ui/core/styles';
import SearchIcon from '@material-ui/icons/Search';
import EditIcon from '@material-ui/icons/Edit';
import AddCircleOutlineIcon from '@material-ui/icons/AddCircleOutline';

import CustomLink from '../widget/CustomLink';
import LoadingSpinner from '../widget/Loading';
import ErrorSign from '../widget/ErrorSign';
import CardSymbol from '../mtg/common/CardSymbol';
import DeckItem from '../mtg/deck/DeckItem';
import DeckForm from '../mtg/deck/DeckForm';
import {
  createDeck,
  fetchDecks,
  deleteDeck,
} from '../../actions';
import { setPageTitle } from '../../shared/utils';
import { DEFAULT_AVATAR } from '../../shared/given';


const styles = theme => ({
  container: {
    padding: theme.spacing(1),
  },
  gridContainer: {

  },
  paperContent: {
    padding: theme.spacing(1),
  },
  cardProfile: {
    textAlign: 'center',
    padding: theme.spacing(2),
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
  },
  cardAvatarContainer: {
    margin: theme.spacing(2),
    width: '60%',
    position: 'relative',
    paddingTop: '60%',
  },
  cardAvatar: {
    width: '100%',
    height: '100%',
    position: 'absolute',
    top: 0,
    bottom: 0,
    left: 0,
    right: 0,
  },
  cardFavBlock: {
    margin: `${theme.spacing(1)}px 0`,
  },
  cardFavSymbol: {
    width: theme.spacing(5),
    height: theme.spacing(5),
  },
  cardFavDeck: {
    margin: `${theme.spacing(0.5)}px 0`,
  },
  cardDivider: {
    backgroundColor: '#AAA',
  },
  cardItemTitle: {
    fontWeight: 700,
    cursor: 'pointer',
  },
  paperContentDivider: {
    margin: theme.spacing(1),
  },
  contentNavis: {
    display: 'flex',
    flexDirection: 'row',
    alignItems: 'stretch',
  },
  contentNaviGrid: {
    textAlign: 'center',
  },
  contentDecks: {
    display: 'flex',
    flexDirection: 'row',
    alignItems: 'stretch',
  },
  contentNaviButton: {
    width: '80%',
    fontFamily: "Lucida Sans Unicode, Lucida Grande, sans-serif",
    height: '100%',
    padding: theme.spacing(1),
  },
  contentNaviButtonText: {
    padding: 0,
    paddingLeft: theme.spacing(1),
    wordBreak: 'break-word',
  },
  symbolic: {
    width: theme.spacing(5),
    height: theme.spacing(5),
  },
  modalTitle: {
    paddingBottom: 0,
  },
});


const getFavoriteDecks = decks => {
  if (!decks?.length) {
    return decks;
  }

  let favDecks = decks.sort(
    (a, b) => a.visited < b.visited
  );
  if (favDecks.length >= 4) {
    favDecks = favDecks.slice(0, 3);
  }

  return favDecks;
};


class MagicBoard extends React.Component {
  state = {
    fetchDeckError: null,
    openCreate: false,
    openDelete: false,
    deckToDelete: null
  };

  handleOpenCreate = () => {
    this.setState({ openCreate: true });
  };

  handleCloseCreate = () => {
    this.setState({ openCreate: false });
  };

  handleOpenDelete = deck => event => {
    this.setState({
      openDelete: true,
      deckToDelete: deck,
    });
  };

  handleCloseDelete = () => {
    this.setState({
      openDelete: false,
      deckToDelete: null,
    });
  };

  componentDidMount() {
    setPageTitle(this.props.title);
    this.props.fetchDecks()
      .catch(
        error => this.setState({ fetchDeckError: error })
      );
  }

  onSubmitDeckCreate = formValues => {
    const { createDeck, user } = this.props;
    const { deckName, deckDescription, deckAvatar, deckFormat, deckImaginary } = formValues;

    createDeck(
      {
        name: deckName,
        description: deckDescription,
        avatar: deckAvatar,
        owner: user._id,
        format: deckFormat,
        imaginary: deckImaginary,
        colors: [],
      })
      .then(
        response => console.log(response)
      )
      .catch(
        error => console.log(error)
      )
      .finally(
        () => this.handleCloseCreate()
      );
  };

  onSubmitDeckDelete = event => {
    const { deckToDelete } = this.state;

    if (!deckToDelete) return;

    const { deleteDeck } = this.props;
    deleteDeck(deckToDelete._id)
      .then(
        response => console.log(response)
      )
      .catch(
        error => console.log(error)
      )
      .finally(
        () => this.handleCloseDelete()
      );
  };

  /**
   * @todo: Create DeckFavorite component
   */
  renderFavoriteDeck = deck => {
    const { classes } = this.props;

    return (
      <div className={classes.cardFavDeck}>
        <div>
          <Typography variant="subtitle1" className={classes.cardItemTitle}>
            {deck.name}
          </Typography>
        </div>
        <CardSymbol type="W" size="small" />
        <CardSymbol type="B" size="small" />
        <div>
          <Typography variant="caption">
            {deck.description}
          </Typography>
        </div>
      </div>
    );
  };

  render() {
    const { user, decks, classes } = this.props;
    const { openCreate, openDelete, deckToDelete } = this.state;

    let favoriteDecks = getFavoriteDecks(decks);

    if (!user) {
      return <LoadingSpinner />;
    }

    return (
      <>
        <Container className={classes.container} maxWidth='xl'>
          <Grid
            container
            direction="row"
            spacing={2}
            className={classes.gridContainer}
          >
            <Hidden only={['xs', 'sm']}>
              <Grid item md={3}>
                <div id="board__user">
                  <Card className={classes.cardProfile}>
                    <Typography variant="h4" component="h4">
                      {user.realName}
                    </Typography>
                    <div className={classes.cardAvatarContainer}>
                      <Avatar
                        className={classes.cardAvatar}
                        alt={user.username}
                        src={user.avatar || DEFAULT_AVATAR}
                      />
                    </div>
                    <div className={classes.cardFavBlock}>
                      <Typography className={classes.cardItemTitle} variant="button">
                        Favorite Color(s)
                      </Typography>
                      <div>
                        <CardSymbol
                          type="R"
                          size="medium"
                        />
                        <CardSymbol
                          type="U"
                          size="medium"
                        />
                      </div>
                    </div>
                    <Divider className={classes.cardDivider} style={{ width: '100%' }}/>
                    <div className={classes.cardFavBlock}>
                      <Typography className={classes.cardItemTitle} variant="button">
                        Favorite Deck(s)
                      </Typography>
                      <div id="board__user__fav-decks">
                        {favoriteDecks.map(
                          (deck, i) => (
                            <React.Fragment key={deck._id}>
                              {i > 0 &&
                                <Divider className={classes.cardDivider} variant="middle" />
                              }
                              {this.renderFavoriteDeck(deck)}
                            </React.Fragment>
                          )
                        )}
                      </div>
                    </div>
                  </Card>
                </div>
              </Grid>
            </Hidden>

            <Grid item sm={12} md={9}>
              <Paper className={classes.paperContent}>
                <div className={classes.contentNavis}>
                  <Grid
                    item xs={4}
                    className={classes.contentNaviGrid}
                  >
                    <Button
                      className={classes.contentNaviButton}
                      variant="contained"
                      color="primary"
                      component={CustomLink}
                      to="/magic/collection"
                    >
                      <SearchIcon />
                      <div className={classes.contentNaviButtonText}>
                        Browse collection
                      </div>
                    </Button>
                  </Grid>
                  <Grid
                    item xs={4}
                    className={classes.contentNaviGrid}
                  >
                    <Button
                      className={classes.contentNaviButton}
                      variant="contained"
                      color="secondary"
                      component={CustomLink}
                      to="/magic/collection-edit"
                    >
                      <EditIcon />
                      <div className={classes.contentNaviButtonText}>
                        Edit collection
                      </div>
                    </Button>
                  </Grid>
                  <Grid
                    item xs={4}
                    className={classes.contentNaviGrid}
                  >
                    <Button
                      className={classes.contentNaviButton}
                      variant="contained"
                      color="primary"
                      onClick={this.handleOpenCreate}
                    >
                      <AddCircleOutlineIcon />
                      <div className={classes.contentNaviButtonText}>
                        Create a deck
                      </div>
                    </Button>
                  </Grid>
                </div>
                <Divider className={classes.paperContentDivider} />
                <div className="board__content__decks">
                  <Grid
                    container
                    spacing={2}
                  >
                    {!this.state.fetchDeckError
                      ?
                      (decks.map(
                        (deck, i) => (
                          <Grid
                            item
                            xs={12}
                            sm={6}
                            key={deck._id}
                          >
                            <DeckItem
                              deck={deck}
                              onDelete={this.handleOpenDelete(deck)}
                            />
                          </Grid>
                        )
                      ))
                      :
                      (
                        <ErrorSign message="Can not fetch user decks" />
                      )
                    }
                  </Grid>
                </div>
              </Paper>
            </Grid>
          </Grid>
        </Container>

        <Dialog
          open={openCreate}
          onClose={this.handleCloseCreate}
          aria-labelledby="form__deck-create"
        >
          <DialogTitle
            className={classes.modalTitle}
            id="form__deck-create"
          >
            Create a deck
          </DialogTitle>
          <DialogContent>
            <DialogContentText style={{ margin: 0 }}>
              Please provide these basic information for the new deck
            </DialogContentText>
            <DeckForm onSubmit={this.onSubmitDeckCreate} />
          </DialogContent>
        </Dialog>
        <Dialog
          open={openDelete}
          onClose={this.handleCloseDelete}
          aria-labelledby="form__deck-delete"
          aria-describedby="form__deck-delete__description"
        >
          <DialogTitle
            className={classes.modalTitle}
            id="form__deck-delete"
          >
            Delete
          </DialogTitle>
          <DialogContent>
            <DialogContentText id="form__deck-delete__description">
              {`Are you sure you want to delete ${deckToDelete?.name || ""}`}
            </DialogContentText>
          </DialogContent>
          <DialogActions>
            <Button onClick={this.onSubmitDeckDelete} color="primary">
              Yes
            </Button>
            <Button onClick={this.handleCloseDelete} color="primary">
              No
            </Button>
          </DialogActions>
        </Dialog>
      </>
    );
  }
}

const mapStateToProps = state => ({
  user: state.auth.user,
  decks: state.deck.decks,
});

const StyledMagicBoard = withStyles(styles)(MagicBoard);

export default connect(mapStateToProps, {
  createDeck,
  fetchDecks,
  deleteDeck,
})(StyledMagicBoard);
