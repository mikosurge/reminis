import React from 'react';

import { withStyles } from '@material-ui/core/styles';

import {
  Button,
  Container,
  Grid,
} from '@material-ui/core';

import { connect } from 'react-redux';

import PostView from '../reader/view/PostView';

import {
  fetchPosts,
  fetchPost,
  fetchRandomPost,
} from '../../actions';

import LoadingSpinner from '../widget/Loading';


const styles = theme => ({
  container: {
    // fontFamily: '"Times New Roman", Times, serif',
    fontFamily: 'Arial, Helvetica, sans-serif',
    padding: 0,
    '& img': {
      maxWidth: '100%',
    },
    height: 'calc(100% - 48px)',
  },
  gridContainer: {

  },
  paper: {

  },
});


class ReaderBoard extends React.Component {
  state = {
    isSubmitting: false,
    currentPost: null,
  };

  componentDidMount() {
    const { fetchPosts } = this.props;

    fetchPosts()
      .then(
        result => {
          const randomIndex = ~~(Math.random() * this.props.posts.length);
          const randomPost = this.props.posts[randomIndex];
          this.setState({
            currentPost: randomPost,
          });
        }
      )
      .catch(
        error => console.log(error)
      );
  }

  handleOnReload = () => {
    const randomIndex = ~~(Math.random() * this.props.posts.length);
    const randomPost = this.props.posts[randomIndex];
    this.setState({
      currentPost: randomPost,
    });
  };

  handleOnSettings = () => {

  };

  render() {
    const { classes, posts } = this.props;
    const { isSubmitting, currentPost } = this.state;

    if (!currentPost) {
      return <LoadingSpinner message={"Fetching message..."}/>
    }

    return (
      <Container
        className={classes.container}
        maxWidth="xl"
        aria-describedby="progress_submitting"
        aria-busy={isSubmitting}
      >
        <PostView
          post={this.state.currentPost}
          onReload={this.handleOnReload}
          onSettings={this.handleOnSettings}
        />
      </Container>
    );
  }
}

const StyledReaderBoard = withStyles(styles)(ReaderBoard);

const mapStateToProps = state => ({
  posts: state.reader.posts,
  currentPost: state.reader.currentPost,
});

export default connect(mapStateToProps, {
  fetchPosts,
  fetchPost,
  fetchRandomPost,
})(StyledReaderBoard);