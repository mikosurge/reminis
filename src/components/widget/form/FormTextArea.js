import React from 'react';

import { makeStyles } from '@material-ui/core/styles';

import {
  FormControl,
  TextField,
} from '@material-ui/core';


const useStyles = makeStyles(theme => ({
  root: {
    margin: theme.spacing(1, 0),
  },
  title: {
    fontWeight: 700,
  },
}));


const FormTextArea = props => {
  const {
    classes,
    id,
    input,
    label,
    meta: { touched, error, },
    helperText,
    required,

    rows,
    handleChange,
    controlledValue,
    disabled,
  } = props;

  const _classes = Object.assign(useStyles(), classes);

  return (
    <TextField
      {...input}
      multiline
      className={_classes.root}
      id={id}
      label={label}
      InputLabelProps={{ className: _classes.title }}
      helperText={touched ? error || 'Looks good!' : helperText}
      error={touched && !!error}
      rows={rows}
      required={required}
      onChange={handleChange?.(id) || input.onChange}
      value={controlledValue || input.value}
      disabled={disabled}
    />
  );
};

export default FormTextArea;