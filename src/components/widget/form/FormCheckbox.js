import React from 'react';

import { makeStyles } from '@material-ui/core/styles';

import {
  Checkbox,
  FormControl,
  FormControlLabel,
  FormHelperText,
} from '@material-ui/core';


const useStyles = makeStyles(theme => ({
  root: {
    margin: theme.spacing(1, 0),
  },
  label: {
    margin: 0,
  },
}));


const FormCheckbox = props => {
  const {
    classes,
    formId,

    id,
    input,
    label,
    defaultChecked,
    helperText,
    labelPlacement,
    required,
    handleChange,
    controlledValue,
  } = props;

  const _classes = Object.assign(useStyles(), classes);

  return (
    <FormControl
      className={_classes.root}
    >
      <FormControlLabel
        {...input}
        className={_classes.label}
        control={
          <Checkbox
            defaultChecked={controlledValue || defaultChecked}
            onChange={handleChange?.(id) || input.onChange}
            value={controlledValue || input.value}
          />
        }
        label={label}
        labelPlacement={labelPlacement || "end"}
      />
      {helperText &&
        <FormHelperText
          id={`${formId}__${id}__helper`}
        >
          {helperText}
        </FormHelperText>
      }
    </FormControl>
  );
};

export default FormCheckbox;