import React from 'react';

import { makeStyles } from '@material-ui/core/styles';

import {
  FormControl,
  TextField,
} from '@material-ui/core';


const useStyles = makeStyles(theme => ({
  title: {
    fontWeight: 700,
  },
  root: {
    margin: theme.spacing(1, 0),
  },
}));


/**
 * @description Represent an input of text, following MUI and redux-form.
 * The style classes passed needs to have 'formControl' and 'textField' parts.
 * The id is encouraged to be same as name of redux form, so that the onChange
 * can be triggered properly.
 * @param {Object} props - All props derived from the parent component
 */
const FormTextField = props => {
  const {
    classes,
    variant,

    id,
    input,
    label,
    type,
    meta: {
      touched, error,
    },
    helperText,
    required,
    handleChange,
    controlledValue,
  } = props;

  let _class = Object.assign(useStyles(), classes);

  return (
    <TextField
      {...input}
      className={_class.root}
      id={id}
      label={label}
      type={type}
      variant={variant}
      InputLabelProps={{ className: _class.title }}
      helperText={touched ? error || 'Looks good!' : helperText}
      error={touched && !!error}
      required={required}
      onChange={handleChange?.(id) || input.onChange}
      value={controlledValue || input.value}
    />
  );
};

export default FormTextField;