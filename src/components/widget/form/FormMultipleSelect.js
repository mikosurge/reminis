import React from 'react';

import { makeStyles } from '@material-ui/core/styles';

import {
  Chip,
  Checkbox,
  FormControl,
  FormHelperText,
  Input,
  InputLabel,
  MenuItem,
  ListItemText,
  Select,
} from '@material-ui/core';

const useStyles = makeStyles(theme => ({
  root: {
    margin: theme.spacing(1, 0),
  },
  title: {
    fontWeight: 700,
  },
  chips: {
    display: 'flex',
    flexWrap: 'wrap',
  },
  chip: {
    margin: 2,
    backgroundColor: theme.palette.primary.main,
    color: 'white',
  },
}));


/**
 * @description Represents a multiple select box.
 * options exists in the form of { text, value }.
 * @param {Object} props -
 */
const FormMultipleSelect = props => {
  const {
    classes,
    formId,

    id,
    input,
    label,
    meta: {
      touched, error,
    },
    helperText,
    required,
    options,
    handleChange,
    controlledValue,
  } = props;

  const _classes = Object.assign(useStyles(), classes);
  const inputId=`${formId}__${id}`;

  return (
    <FormControl
      className={_classes.root}
      required={required}
    >
      <InputLabel
        className={_classes.title}
        id={`${inputId}__label`}
      >
        {label}
      </InputLabel>
      <Select
        {...input}
        labelid={`${inputId}__label`}
        id={inputId}
        multiple
        input={<Input id={`${formId}__chip`} />}
        renderValue={
          selected => (
            <div className={_classes.chips}>
              {selected.map(value => <Chip key={value} label={value} className={_classes.chip} />)}
            </div>
          )
        }
        required
        onChange={handleChange?.(id) || input.onChange}
        value={controlledValue || input.value || []}
      >
        {
          options.map(
            option => (
              <MenuItem key={option.text} value={option.text}>
                <Checkbox checked={input.value.includes(option.text)} />
                <ListItemText primary={option.text} />
              </MenuItem>
            )
          )
        }
      </Select>
      <FormHelperText
        id={`${inputId}__helper`}
      >
        {
          touched
          ? error || 'Looks good!'
          : helperText
        }
      </FormHelperText>
    </FormControl>
  );
};

export default FormMultipleSelect;