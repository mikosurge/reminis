import React from 'react';

import { makeStyles } from '@material-ui/core/styles';

import {
  FormControl,
  FormControlLabel,
  FormLabel,
  Radio,
  RadioGroup,
} from '@material-ui/core';


const useStyles = makeStyles(theme => ({
  title: {
    fontWeight: 700,
  },
  root: {

  },
}));


const FormRadiobox = props => {
  const {
    classes,

    id,
    input,
    label,
    meta: {
      touched, error,
    },
    required,
    options,
    handleChange,
    controlledValue,
  } = props;

  let _classes = Object.assign(useStyles(), classes);

  return (
    <FormControl
      className={_classes.root}
      component="fieldset"
    >
      <FormLabel
        component="legend"
      >
        {label}
      </FormLabel>
      <RadioGroup
        {...input}
        aria-label={label}
        name={label}
        onChange={handleChange?.(id) || input.onChange}
        value={controlledValue || input.value}
      >
        {options.map(
          option => (
            <FormControlLabel
              key={option.value}
              value={option.value}
              control={<Radio color="primary" />}
              label={option.text}
            />
          )
        )}
      </RadioGroup>
    </FormControl>
  );
};

export default FormRadiobox;