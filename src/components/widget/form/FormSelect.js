import React from 'react';

import { makeStyles } from '@material-ui/core/styles';

import {
  InputLabel,
  FormControl,
  FormHelperText,
  MenuItem,
  Select,
} from '@material-ui/core';


const useStyles = makeStyles(theme => ({
  root: {
    margin: theme.spacing(1, 0),
  },
  title: {
    fontWeight: 700,
  },
}));


const FormSelect = props => {
  const {
    classes,
    formId,

    id,
    input,
    label,
    meta: {
      touched, error,
    },
    helperText,
    required,
    options,
    handleChange,
    controlledValue,
  } = props;

  const inputId = `${formId}__${id}`;
  const _classes = Object.assign(useStyles(), classes);

  return (
    <FormControl
      className={_classes.root}
      error={touched && !!error}
      required={required}
    >
      <InputLabel
        className={_classes.title}
        htmlFor={inputId}
      >
        {label}
      </InputLabel>
      <Select
        {...input}
        name={id}
        id={inputId}
        onChange={handleChange?.(id) || input.onChange}
        value={controlledValue || input.value}
      >
        {options.map(
          option => <MenuItem key={option.value} value={option.value}>{option.text}</MenuItem>
        )}
      </Select>
      <FormHelperText
        id={`${inputId}__helper`}
      >
        {
          touched
          ? error || 'Looks good!'
          : helperText
        }
      </FormHelperText>
    </FormControl>
  );
};

export default FormSelect;