import React from 'react';
import ReactMarkdown from 'react-markdown';

import { makeStyles } from '@material-ui/core/styles';

import {
  Typography,
} from '@material-ui/core';


const useStyles = makeStyles(theme => ({
  root: {

  },
  text: {
    fontSize: theme.typography.pxToRem(16),
  },
  break: {

  },
  paragraph: {
    fontSize: theme.typography.pxToRem(16),
    lineHeight: 1.6,
    textAlign: 'justify',
    wordBreak: 'break-word',
  },
  emphasis: {

  },
  strong: {

  },
  thematicBreak: {

  },
  blockquote: {
    borderLeft: '5px solid #a0aabf',
    marginInlineStart: 0,
    "& p": {
      marginInlineStart: '24px',
      paddingTop: '16px',
      paddingBottom: '16px',
    },
    fontStyle: 'italic',
    fontSize: theme.typography.pxToRem(14),
  },
  delete: {

  },
  link: {

  },
  image: {

  },
  linkReference: {

  },
  imageReference: {

  },
  table: {

  },
  tableHead: {

  },
  tableBody: {

  },
  tableRow: {

  },
  list: {
    fontSize: theme.typography.pxToRem(16),
  },
  listItem: {

  },
  definition: {

  },
  heading: {

  },
  inlineCode: {

  },
  code: {

  },
  html: {

  },
}));

function getCoreProps(props) {
  return props['data-sourcepos'] ? {
    'data-sourcepos': props['data-sourcepos']
  } : {};
}

function List(props) {
  var attrs = getCoreProps(props);

  if (props.start !== null && props.start !== 1 && props.start !== undefined) {
    attrs.start = props.start.toString();
  }

  return React.createElement(props.ordered ? 'ol' : 'ul', attrs, props.children);
}

function ListItem(props) {
  var checkbox = null;

  if (props.checked !== null && props.checked !== undefined) {
    var checked = props.checked;
    checkbox = React.createElement('input', {
      type: 'checkbox',
      checked: checked,
      readOnly: true
    });
  }

  return React.createElement('li', getCoreProps(props), checkbox, props.children);
}

const Markdown = props => {
  const classes = useStyles();
  const { source, customClasses } = props;
  const _classes = { ...classes, ...customClasses };

  const _renderers = {
    paragraph: props => <p className={_classes.paragraph} {...props} />,
    blockquote: props => <blockquote className={_classes.blockquote} {...props} />,
    list: props => <List className={_classes.list} {...props} />,
    listItem: props => <ListItem className={_classes.listItem} {...props} />,
  };

  return (
    <ReactMarkdown
      source={source}
      renderers={_renderers}
    />
  );
}

export default Markdown;