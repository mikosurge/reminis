import React from 'react';
import clsx from 'clsx';

import { makeStyles } from '@material-ui/core/styles';

import {
  Typography,
} from '@material-ui/core';

import ReportProblemRoundedIcon from '@material-ui/icons/ReportProblemRounded';

const useStyles = makeStyles(theme => ({
  container: {
    display: 'flex',
    flexDirection: 'column',
    textAlign: 'center',
    justifyContent: 'center',
    alignItems: 'center'
  },
  icon: {
    color: theme.palette.secondary.main,
    borderRadius: 500,
  },
  iconBig: {
    fontSize: theme.typography.pxToRem(128),
  },
  iconMedium: {
    fontSize: theme.typography.pxToRem(64),
  },
  iconSmall: {
    fontSize: theme.typography.pxToRem(32),
  },
  text: {
    color: theme.palette.secondary.main,
  },
}));


export default function ErrorSign(props) {
  const classes = useStyles();
  const { size, noText, message, margin } = props;

  let _clazz = classes.iconBig;
  let _variant = "h6";
  let _message = message || "An error occurred...";
  let _height = 40;

  if (size === 'small') {
    _clazz = classes.iconSmall;
    _variant = "subtitle2";
    _height = 20;
  } else if (size === 'medium') {
    _clazz = classes.iconMedium;
    _variant = "subtitle2";
    _height = 30;
  }

  return (
    <div
      className={classes.container}
      style={{
        // height: `${_height}vh`,
        margin: `${margin} 0`,
      }}
    >
      <ReportProblemRoundedIcon
        className={clsx(classes.icon, _clazz)}
      />
      {!noText &&
        <Typography
          className={classes.text}
          align={"center"}
          color={"primary"}
          variant={_variant}
        >
          {_message}
        </Typography>
      }
    </div>
  );
};