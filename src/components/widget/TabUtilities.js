import React from 'react';
import {
  Typography,
} from '@material-ui/core';


export const TabPanel = (props) => {
  const { category, children, value, index, ...other } = props;

  return (
    <Typography
      component="div"
      role="tabpanel"
      hidden={value !== index}
      id={`scrollable-tabpanel-${category}-${index}`}
      {...other}
    >
      {children}
    </Typography>
  );
};

export function a11yProps(category, index) {
  return {
    id: `scrollable-tab-${category}-${index}`,
    'aria-controls': `scrollable-tabpanel-${category}-${index}`,
  };
};