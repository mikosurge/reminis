import React from 'react';

import { makeStyles } from '@material-ui/core/styles';

import {
  CircularProgress,
  Typography,
} from '@material-ui/core';

const useStyles = makeStyles(theme => ({
  container: {
    display: 'flex',
    flexDirection: 'column',
    textAlign: 'center',
    justifyContent: 'center',
    alignItems: 'center'
  },
  spinner: {

  },
  text: {
    marginTop: theme.spacing(4),
    marginBottom: theme.spacing(4),
  },
}));

export default function LoadingSpinner(props) {
  const classes = useStyles();
  const { size, noText, message } = props;

  let _size = 144;
  let _thickness = 3.6;
  let _variant = "h6";
  let _message = message || "Kindly wait when we fetch some data...";
  let _height = 60;

  if (size === 'small') {
    _size = 36;
    _thickness = 1;
    _variant = "subtitle2";
    _height = 30;
  } else if (size === 'medium') {
    _size = 72;
    _thickness = 1.8;
    _variant = "subtitle2";
    _height = 40;
  }

  return (
    <div
      className={classes.container}
      style={{
        height: `${_height}vh`,
      }}
    >
      <CircularProgress
        className={classes.spinner}
        size={_size}
        thickness={_thickness}
      />
      {!noText &&
        <Typography
          className={classes.text}
          align={"center"}
          color={"primary"}
          variant={_variant}
        >
          {_message}
        </Typography>
      }
    </div>
  );
};