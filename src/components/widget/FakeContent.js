import React from 'react';

import { makeStyles } from '@material-ui/core/styles';

import { grey } from '@material-ui/core/colors';

import { range } from '../../shared/utils';


const useStyles = makeStyles(theme => ({
  container: {
    // Added 1px so that the margin won't be collapsed together
    padding: `1px ${theme.spacing(2)}px`,
  },
  fake: {
    backgroundColor: grey[200],
    height: theme.spacing(1),
    margin: theme.spacing(2),
    '&:nth-child(2n)': {
      marginRight: theme.spacing(3),
    },
  },
}));


export default function FakeContent(props) {
  const classes = useStyles();

  let lines = props?.lines || 5;
  let Wrapper = props?.wrapper;

  if (Wrapper) {
    return (
      <Wrapper className={classes.container}>
        {range(lines).map(
          i => <div key={i} className={classes.fake} />
        )}
      </Wrapper>
    );
  } else {
    return (
      <>
        {range(lines).map(
          i => <div key={i} className={classes.fake} />
        )}
      </>
    );
  }
};