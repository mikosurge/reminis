import React from 'react';
import clsx from 'clsx';

import { makeStyles } from '@material-ui/core/styles';

import {
  Typography,
} from '@material-ui/core';

import DoneIcon from '@material-ui/icons/Done';


const useStyles = makeStyles(theme => ({
  container: {
    display: 'flex',
    flexDirection: 'column',
    textAlign: 'center',
    justifyContent: 'center',
    alignItems: 'center'
  },
  icon: {
    color: 'white',
    backgroundColor: 'green',
    borderRadius: 500,
  },
  iconBig: {
    fontSize: theme.typography.pxToRem(128),
    padding: theme.spacing(1),
  },
  iconMedium: {
    fontSize: theme.typography.pxToRem(64),
    padding: theme.spacing(1),
  },
  iconSmall: {
    fontSize: theme.typography.pxToRem(32),
    padding: theme.spacing(0.5),
  },
  text: {
    // marginTop: theme.spacing(1),
    // marginBottom: theme.spacing(1),
    color: 'green',
  },
}));


export default function ConfirmSign(props) {
  const classes = useStyles();
  const { size, noText, message, margin } = props;

  let _clazz = classes.iconBig;
  let _variant = "h6";
  let _message = message || "Success";

  if (size === 'small') {
    _clazz = classes.iconSmall;
    _variant = "subtitle2";
  } else if (size === 'medium') {
    _clazz = classes.iconMedium;
    _variant = "subtitle2";
  }

  return (
    <div
      className={classes.container}
      style={{
        margin: `${margin} 0`,
      }}
    >
      <DoneIcon
        className={clsx(classes.icon, _clazz)}
      />
      {!noText &&
        <Typography
          className={classes.text}
          align={"center"}
          color={"primary"}
          variant={_variant}
        >
          {_message}
        </Typography>
      }
    </div>
  );
};