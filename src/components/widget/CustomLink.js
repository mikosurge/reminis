import React from 'react';
import { Link as RouterLink } from 'react-router-dom';

const CustomLink = React.forwardRef(
  (props, ref) => {
    return (
      <RouterLink
        innerRef={ref}
        to={props.to}
        {...props}
      />
    );
  }
);

export default CustomLink;