/**
 * @description This example shows a proper way to make use of /widget/NotificationBar.
 * The component is based on Material UI Snackbar, which you should specify its own handlers
 * to open and close itself, by giving the correct format of variant and message.
 */
import React from 'react';

import { Button } from '@material-ui/core';

import NotificationBar from '../NotificationBar';
import { randomInRange } from '../../../shared/utils';


const snackbarVariants = [
  {
    variant: 'error',
    message: "This is an error message!",
  },
  {
    variant: 'warning',
    message: "This is a warning message!",
  },
  {
    variant: 'info',
    message: "This is an information message!",
  },
  {
    variant: 'success',
    message: "This is a success message!",
  },
];

const anchorOrigin = {
  vertical: 'top',
  horizontal: 'center',
};

class Notification extends React.Component {
  state = {
    openSnackbar: false,
    snackbarProps: snackbarVariants[0],
  };

  triggerNotification = ({ variant, message }) => {
    this.setState({
      openSnackbar: true,
      snackbarProps: {
        variant,
        message,
      },
    })
  };

  handleSnackbarClose = (event, reason) => {
    if (reason === 'clickaway') {
      return;
    }

    this.setState({ openSnackbar: false })
  };

  handleSnackbarOpen = () => {
    let randomNumber = randomInRange(0, 3);
    const randomSnackbar = snackbarVariants[randomNumber];
    this.triggerNotification(randomSnackbar);
  };

  render() {
    return (
      <>
        <Button
          variant="contained"
          color="primary"
          onClick={this.handleSnackbarOpen}
        >
          Open Snackbar
        </Button>
        <NotificationBar
          variant={this.state.snackbarProps.variant}
          onClose={this.handleSnackbarClose}
          message={this.state.snackbarProps.message}
          anchorOrigin={anchorOrigin}
          open={this.state.openSnackbar}
          autoHideDuration={3000}
        />
      </>
    );
  }
};

export default Notification;