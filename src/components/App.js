import React from 'react';
import { Router, Route, Switch } from 'react-router-dom';
import { connect } from 'react-redux';

import { withStyles, createMuiTheme } from '@material-ui/core/styles';
import { ThemeProvider } from '@material-ui/styles';
import { CssBaseline } from '@material-ui/core';

import history from '../history';
import PrivateRoute from './auth/PrivateRoute';

import AuthForm from './auth/AuthForm';
import HomePage from './widget/HomePage';
import SignUpForm from './auth/SignUpForm';

import { authGuard } from '../actions';
import MockBoard from './board/MockBoard';

const theme = createMuiTheme({
  typography: {
    fontFamily: "\"Trebuchet MS\", Helvetica, sans-serif",
  },
});

// Any customized style you want to put here
const styles = theme => ({
  // '@global': {
  //   body: {
  //     fontSize: '16px',
  //   },
  // },
});

class App extends React.Component {
  constructor(props) {
    super(props);
    this.props.authGuard();
  }

  render() {
    return (
      <ThemeProvider theme={theme}>
        <CssBaseline />
        <Router history={history}>
          <Switch>
            <Route exact path="/auth" component={AuthForm} />
            <Route exact path="/signup" component={SignUpForm} />
            <Route exact path="/mock" component={MockBoard} />
            <PrivateRoute path="/" component={HomePage} />
          </Switch>
        </Router>
      </ThemeProvider>
    );
  };
}

const mapStateToProps = state => ({
  auth: state.auth,
});

const StyledApp = withStyles(styles)(App);

export default connect(mapStateToProps, {
  authGuard,
})(StyledApp);