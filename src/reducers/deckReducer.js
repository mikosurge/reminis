import {
  FETCH_DECK,
  FETCH_DECKS,
  CREATE_DECK,
  EDIT_DECK,
  DELETE_DECK,
} from '../actions/types';


const INITIAL_STATE = {
  decks: [],
  currentDeck: {}
};


export default (state = INITIAL_STATE, action) => {
  switch (action.type) {
    case FETCH_DECKS:
      return { ...state, decks: action.payload };
    case FETCH_DECK:
      return { ...state, currentDeck: action.payload };
    case CREATE_DECK:
      return { ...state, decks: [ ...state.decks, action.payload ] };
    case EDIT_DECK:
      return { ...state, currentDeck: action.payload };
    case DELETE_DECK:
      return { ...state, decks: state.decks.filter(
        deck => deck._id !== action.payload
      )};
    default:
      return state;
  }
};