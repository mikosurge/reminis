import { combineReducers } from 'redux';
import { reducer as formReducer } from 'redux-form';

import authReducer from './authReducer';
import collectionReducer from './collectionReducer';
import deckReducer from './deckReducer';
import cardReducer from './cardReducer';
import changeReducer from './changeReducer';
import glossaryReducer from './glossaryReducer';
import readerReducer from './readerReducer';

export default combineReducers({
  auth: authReducer,
  form: formReducer,
  cards: cardReducer,
  collection: collectionReducer,
  deck: deckReducer,
  changes: changeReducer,
  glossary: glossaryReducer,
  reader: readerReducer,
});