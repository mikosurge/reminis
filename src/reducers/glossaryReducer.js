import {
  FETCH_ENGLISH_WORDS,
  EDIT_ENGLISH_WORD,
} from '../actions/types';

import { isEmptyObject } from '../shared/utils';

const INITIAL_STATE = {
  currentWord: { },
  currentPhrase: { },
  words: [
    {
      word: 'simple',
    },
    {
      word: 'complex',
    }
  ],
  phrases: [ ],
};


export default (state = INITIAL_STATE, action) => {
  let newState = null;
  let index = null;
  switch (action.type) {
    case FETCH_ENGLISH_WORDS:
      return { ...state, words: action.payload };
    case EDIT_ENGLISH_WORD:
      let newWords = state.words;
      let newWord = action.payload;
      index = newState.findIndex(_word => _word._id === newWord._id);
      if (!~index) {
        newWords[index] = newWord;
      }
      return { ...state, words: newWords };
    default:
      return state;
  }
};