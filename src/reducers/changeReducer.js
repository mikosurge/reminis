import {
  EDIT_CHANGE,
  CLEAR_CHANGE,
} from '../actions/types';


/**
 * @description Store a card list of changes, from either collection or deck.
 * One change unit will have an action, which can be "insert", "update" or "remove".
 * The number of cards has to be positive if the action is "insert" or "update",
 * and has to be negative if the action is "update" or "remove".
 */
export default (state = [], action) => {
  switch (action.type) {
    case EDIT_CHANGE:
      if (!state.length) {
        return [...state, action.payload ];
      } else {
        let isExisting = false;
        let newState = [];
        const { card, foil, num, perform, isMain } = action.payload;

        for (let i = 0; i < state.length; i++) {
          const _unit = state[i];
          if (_unit.card._id === card._id && _unit.foil === foil && _unit.isMain === isMain) {
            isExisting = true;
            if (_unit.num + num === 0) {
              // After changing, there will be none of this card
              newState = state.filter(
                unit => (unit.card._id !== card._id || unit.foil !== foil || unit.isMain !== isMain)
              );
            } else {
              newState = state.slice();
              newState[i].num += num;
              newState[i].perform = perform;
            }
            return newState;
          }
        }

        if (!isExisting) {
          return [ ...state, action.payload ];
        } else {
          return state;
        }
      }
    case CLEAR_CHANGE:
      return [];
    default:
      return state;
  }
};