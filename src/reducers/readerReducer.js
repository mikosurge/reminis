import {
  FETCH_POST_MESSAGES,
  FETCH_POST_MESSAGE,
  EDIT_POST_MESSAGE,
  SAVE_POST_MESSAGE,
  DELETE_POST_MESSAGE,
} from '../actions/types';

const INITIAL_STATE = {
  posts: [],
  currentPost: { },
};


export default (state = INITIAL_STATE, action) => {
  let newState = null;
  let index = null;
  switch (action.type) {
    case FETCH_POST_MESSAGES:
      return { ...state, posts: action.payload };
    case FETCH_POST_MESSAGE:
      newState = state.slice();
      index = newState.posts.findIndex(_post => _post._id === action.payload._id);
      newState[index] = action.payload;
      return newState;
    case EDIT_POST_MESSAGE:
      return { ...state, currentPost: action.payload };
    case DELETE_POST_MESSAGE:
      return state;
    default:
      return state;
  }
};