import {
  FETCH_ENGLISH_WORDS,
  EDIT_ENGLISH_WORD,
  CREATE_ENGLISH_WORD,
} from './types';

import local from '../apis/local';


export const fetchEnglishWords = () => async (dispatch, getState) => {
  const { user: { username, }, token } = getState().auth;

  const response = await local.get('/glossary/english/words', {
    headers: {
      'Authorization': `Bearer ${token}`,
    },
  });

  if (response?.data) {
    let words = response.data;
    return dispatch({
      type: FETCH_ENGLISH_WORDS,
      payload: words,
    });
  }
};


export const saveEnglishWord = word => async(dispatch, getState) => {
  const { user: { username, }, token } = getState().auth;
  const { _id } = word;

  const response = await local.post(`/glossary/english/word/${_id}`,
    word,
    {
      headers: {
        'Authorization': `Bearer ${token}`,
      }
    }
  );

  if (response?.data) {
    return dispatch({
      type: EDIT_ENGLISH_WORD,
      payload: word,
    });
  }
};

export const createEnglishWord = word => async(dispatch, getState) => {
  const { user: { username, }, token } = getState().auth;

  const response = await local.post('/glossary/english/word',
    word,
    {
      headers: {
        'Authorization': `Bearer ${token}`,
      }
    }
  );

  if (response?.data) {
    return dispatch({
      type: CREATE_ENGLISH_WORD,
      payload: word,
    });
  }
};