import {
  FETCH_POST_MESSAGES,
  FETCH_POST_MESSAGE,
  EDIT_POST_MESSAGE,
  SAVE_POST_MESSAGE,
  DELETE_POST_MESSAGE,
} from './types';

import local from '../apis/local';

export const fetchPosts = () => async (dispatch, getState) => {
  const { user: { username }, token } = getState().auth;

  const response = await local.get('/reader/post-messages', {
    headers: {
      'Authorization': `Bearer ${token}`,
    },
  });

  if (response?.data) {
    let posts = response.data;
    return dispatch({
      type: FETCH_POST_MESSAGES,
      payload: posts,
    });
  }
};

export const fetchPost = postId => async (dispatch, getState) => {

};

export const fetchRandomPost = () => async (dispatch, getState) => {

};

export const editPost = post => {

};

export const savePost = post => async (dispatch, getState) => {

};

export const deletePost = postId => async (dispatch, getState) => {

};