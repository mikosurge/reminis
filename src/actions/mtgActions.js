import {
  FETCH_DECK,
  FETCH_DECKS,
  CREATE_DECK,
  DELETE_DECK,
  FETCH_COLLECTION,
  FETCH_CARDS,
} from './types';
import local from '../apis/local';


export const fetchDecks = () => async (dispatch, getState) => {
  const { user: { _id }, token } = getState().auth;
  const response = await local.get(`/mtg/${_id}/decks`, {
    headers: {
      'Authorization': `Bearer ${token}`,
    },
  });

  if (response?.data?.success) {
    return dispatch({
      type: FETCH_DECKS,
      payload: response.data.decks,
    });
  }
};


export const fetchDeck = deckId => async (dispatch, getState) => {
  const { user: { _id }, token } = getState().auth;
  const response = await local.get(`/mtg/${_id}/deck/info/${deckId}`, {
    headers: {
      'Authorization': `Bearer ${token}`,
    },
  });

  if (response?.data?.success) {
    dispatch({
      type: FETCH_DECK,
      payload: response.data.deck,
    });
  }
};


export const createDeck = deckFormValues => async (dispatch, getState) => {
  const { user: { _id }, token } = getState().auth;
  const response = await local.post(`/mtg/${_id}/deck/create`,
    deckFormValues,
    {
      headers: {
        'Content-Type': 'application/json',
        'Authorization': `Bearer ${token}`,
      },
    }
  );

  if (response?.data?.success) {
    return dispatch({
      type: CREATE_DECK,
      payload: response.data.deck,
    });
  }
};


export const deleteDeck = deckId => async (dispatch, getState) => {
  const { user: { _id }, token } = getState().auth;
  const response = await local.delete(`/mtg/${_id}/deck/delete/${deckId}`, {
    headers: {
      'Authorization': `Bearer ${token}`,
    },
  });

  if (response?.data.success) {
    return dispatch({
      type: DELETE_DECK,
      payload: response.data.deck,
    });
  }
};


export const editDeck = deckId => async (dispatch, getState) => {
  const { user: { _id }, token } = getState().auth;
  const { changes } = getState();

  const response = await local.post(`/mtg/${_id}/deck/edit/${deckId}`,
    changes,
    {
      headers: {
        'Content-Type': 'application/json',
        'Authorization': `Bearer ${token}`,
      },
    }
  );

  if (response?.data?.success) {
    return true;
  }

  return false;
};


export const fetchCollection = () => async (dispatch, getState) => {
  const { user: { _id, mtg: { collection } }, token } = getState().auth;

  const response = await local.get(`/mtg/${_id}/collection/info/${collection}`, {
    headers: {
      'Authorization': `Bearer ${token}`,
    }
  });

  if (response?.data?.success) {
    return dispatch({
      type: FETCH_COLLECTION,
      payload: response.data.collection,
    });
  }
};


export const editCollection = () => async (dispatch, getState) => {
  const { user: { _id, mtg: { collection } }, token } = getState().auth;
  const { changes } = getState();

  const response = await local.post(`/mtg/${_id}/collection/edit/${collection}`,
    changes,
    {
      headers: {
        'Content-Type': 'application/json',
        'Authorization': `Bearer ${token}`,
      },
    }
  );

  if (response?.data?.success) {
    return true;
  }
  return false;
};


export const fetchCards = () => async (dispatch, getState) => {
  const { cards } = getState();
  if (cards.length > 0) {
    return;
  }

  const response = await(local.get('/mtg/cards/brief'));

  dispatch({
    type: FETCH_CARDS,
    payload: response.data,
  });
};