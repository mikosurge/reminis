import local from '../apis/local';
import {
  SIGN_IN,
  SIGN_OUT,
} from './types';

export const authGuard = () => {
  return async dispatch => {
    const token = localStorage.getItem("token");
    if (!token) {
      return dispatch({
        type: SIGN_OUT,
      });
    }

    local.get('/auth/unlock',
      {
        headers: {
          'Authorization': `Bearer ${token}`
        }
      })
      .then(
        response => {
          const { user, token, expiresIn } = response.data;
          setTimeout(() => {
            localStorage.removeItem("token");
            dispatch({
              type: SIGN_OUT,
              payload: {}
            });
          }, expiresIn * 1000);

          return dispatch({
            type: SIGN_IN,
            payload: {
              user,
              token,
            }
          });
        }
      )
      .catch(
        err => {
          return dispatch({
            type: SIGN_OUT,
          });
        }
      );
  };
};

export const signUp = (formValues) => {
  return async dispatch => {
    const response = await local.post('auth/signup', formValues);

    if (response.data && response.data.success) {
      const { user, token, expiresIn } = response.data;
      localStorage.setItem("token", token);
      setTimeout(() => {
        localStorage.removeItem("token");
        dispatch({
          type: SIGN_OUT,
          payload: {}
        });
      }, expiresIn * 1000);

      dispatch({
        type: SIGN_IN,
        payload: {
          user,
          token
        }
      });
    } else {
      return response;
    }
  };
};

export const signIn = (username, password) => {
  return async dispatch => {
    const response = await local.post('/auth/login', {
      username,
      password
    });

    if (response.data && response.data.success) {
      const { user, token, expiresIn } = response.data;
      localStorage.setItem("token", token);
      setTimeout(() => {
        localStorage.removeItem("token");
        dispatch({
          type: SIGN_OUT,
          payload: {}
        });
      }, expiresIn * 1000);

      return dispatch({
        type: SIGN_IN,
        payload: {
          user,
          token,
        }
      });
    }
  };
};

export const signOut = () => {
  return async (dispatch, getState) => {
    const { token } = getState().auth;
    const response = await local.get('/auth/logout',
      {
        headers: {
          'Authorization': `Bearer ${token}`
        }
      }
    );

    if (response.data && response.data.success) {
      localStorage.removeItem("token");
      return dispatch({
        type: SIGN_OUT,
        payload: {}
      });
    }
  }
}