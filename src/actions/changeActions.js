import {
  EDIT_CHANGE,
  CLEAR_CHANGE,
} from './types';
import local from '../apis/local';


export const editChanges = unit => ({
  type: EDIT_CHANGE,
  payload: {
    ...unit
  }
});


export const clearChanges = () => ({
  type: CLEAR_CHANGE,
  payload: []
});


export const submitCollectionChanges = () => async (dispatch, getState) => {
  const { user: { _id, mtg: { collection } }, token } = getState().auth;
  const { changes } = getState();

  const response = await local.post(`/mtg/${_id}/collection/modify/${collection}`,
    changes,
    {
      headers: {
        'Content-Type': 'application/json',
        'Authorization': `Bearer ${token}`,
      },
    }
  );

  if (response?.data?.success) {
    return true;
  }

  return false;
};


export const submitDeckChanges = deckId => async (dispatch, getState) => {
  const { user: { _id }, token } = getState().auth;
  const { changes } = getState();

  const response = await local.post(`/mtg/${_id}/deck/modify/${deckId}`,
    changes,
    {
      headers: {
        'Content-Type': 'application/json',
        'Authorization': `Bearer ${token}`,
      },
    }
  );

  if (response?.data?.success) {
    return true;
  }
  return false;
}