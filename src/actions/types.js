// Categories
export const D_AUTH = 'AUTH';
export const D_MTG = 'MTG';
export const D_GLOSSARY = 'GLOSSARY';
export const D_READER = 'READER';


// Authentication
export const              SIGN_IN = `@@${D_AUTH}/SIGN_IN`;
export const             SIGN_OUT = `@@${D_AUTH}/SIGN_OUT`;

// Magic: The Gathering
export const     FETCH_COLLECTION = `@@${D_MTG}/FETCH_COLLECTION`;
export const      EDIT_COLLECTION = `@@${D_MTG}/EDIT_COLLECTION`;
export const           FETCH_CARD = `@@${D_MTG}/FETCH_CARD`;
export const          FETCH_CARDS = `@@${D_MTG}/FETCH_CARDS`;
export const           FETCH_DECK = `@@${D_MTG}/FETCH_DECK`;
export const          FETCH_DECKS = `@@${D_MTG}/FETCH_DECKS`;
export const          CREATE_DECK = `@@${D_MTG}/CREATE_DECK`;
export const            EDIT_DECK = `@@${D_MTG}/EDIT_DECK`;
export const          DELETE_DECK = `@@${D_MTG}/DELETE_DECK`;

export const          EDIT_CHANGE = `@@${D_MTG}/EDIT_CHANGE`;
export const         CLEAR_CHANGE = `@@${D_MTG}/CLEAR_CHANGE`;

// Glossary
export const FETCH_ENGLISH_WORDS = `@@${D_GLOSSARY}/FETCH_ENGLISH_WORDS`;
export const FETCH_JAPANESE_WORDS = `@@${D_GLOSSARY}/FETCH_JAPANESE_WORDS`;
export const FETCH_SYNC_UNIT = `@@${D_GLOSSARY}/FETCH_SYNC_UNIT`;
export const EDIT_ENGLISH_WORD = `@@${D_GLOSSARY}/EDIT_ENGLISH_WORD`;
export const EDIT_JAPANESE_WORD = `@@${D_GLOSSARY}/EDIT_JAPANESE_WORD`;
export const EDIT_SYNC_UNIT = `@@${D_GLOSSARY}/EDIT_SYNC_UNIT`;
export const CREATE_ENGLISH_WORD = `@@${D_GLOSSARY}/CREATE_ENGLISH_WORD`;
export const CREATE_JAPANESE_WORD = `@@${D_GLOSSARY}/CREATE_JAPANESE_WORD`;
export const CREATE_SYNC_UNIT = `@@${D_GLOSSARY}/CREATE_SYNC_UNIT`;

// Reader
export const FETCH_POST_MESSAGES = `@@${D_READER}/FETCH_POST_MESSAGES`;
export const FETCH_POST_MESSAGE = `@@${D_READER}/FETCH_POST_MESSAGE`;
export const EDIT_POST_MESSAGE = `@@${D_READER}/EDIT_POST_MESSAGE`;
export const SAVE_POST_MESSAGE = `@@${D_READER}/SAVE_POST_MESSAGE`;
export const DELETE_POST_MESSAGE = `@@${D_READER}/DELETE_POST_MESSAGE`;