export const SERVER_ENDPOINT = "http://10.205.9.17:3034";
export const DEFAULT_AVATAR = '/iconic.png';
export const DEFAULT_EMBLEM = '/emblem.png';
export const DEFAULT_TITLE = 'Reminis';

export const CARD_SIZES = {
  normal: {
    w: 240,
    h: 334,
  },
  large: {
    w: 336,
    h: 468,
  },
  inch: {
    w: 2.5,
    h: 3.5,
  },
  cm: {
    w: 6.3,
    h: 8.8
  },
};

export const REFINE_BLOCK = {
  SEARCH: 0,
  FILTER: 1,
};

export const UTILITY_BLOCK  = {
  RESULT: 0,
  VIEW: 1,
  STATISTIC: 2,
  SIMULATION: 3,
};

export const LIST_BLOCK = {
  MAIN: 0,
  SIDE: 1,
  ALL: 2,
};