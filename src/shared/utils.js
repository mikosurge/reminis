/**
 * @description Template string implementation, similar to python.
 * The method expect an input string and its key for future replacement.
 *
 * Input string with template is in fact an array contains all of its parts.
 * Other parts in ${} will be stored in keys.
 * Values are passed in sequence, with the last argument of an object, in case some keys are not numeric.
 *
 * @param {String} strings
 * @param  {Object} keys
 * @example: `foo${0}bar${1}baz${'qux'}` => (['foo', 'bar', 'baz', 'qux'], [0, 1, 'qux'])
 * with values = (A, B, { qux: C }) => `fooAbarBbazC`
 */
export const template = (strings, ...keys) => {
  return ((...values) => {
    let dict = values[values.length - 1] || {};
    let result = [strings[0]];
    keys.forEach(
      (key, i) => {
        let value = Number.isInteger(key) ? values[key] : dict[key];
        result.push(value, strings[i + 1]);
      }
    );
    return result.join('');
  });
};


// Double tilde is the substitution for Math.floor()
/**
 * @description Randomize a number that has a given minimum and maximum.
 * Double tilde is the substitution for Math.floor().
 * Since Math.random() gives a Float from [0, 1), (max + 1) is used to guarantee
 * that the output number can reach max.
 * @param {Number} min - The minimum value
 * @param {Number} max - The maximum value
 */
export const randomInRange = (min, max) => {
  return ~~(Math.random() * (max + 1 - min) + min);
};


export const range = (fromOrTo, to, step = 1) => {
  if (!to) {
    return Array.from(Array(fromOrTo).keys());
  }

  let i = fromOrTo;
  const result = [];

  while (i <= to) {
    result.push(i);
    i += step;
  }

  return result;
};


/**
 * @description Get k random elements from array.
 * This implementation refers to the similar topic at https://jsperf.com/k-random-elements-from-array/2.
 * Instead of throwing an error, this impl. will return the source array if num > source.length
 * @param {Array<any>} source - The source array
 * @param {Number} n - The number of items to be retrieved
 */
export const severalRandomsInList = (source, n) => {
  let result = new Array(n), len = source.length, taken = new Array(len);
  if (!source) {
    return [];
  }

  if (n > len) {
    return source;
  }

  while (n--) {
    let x = ~~(Math.random() * len);
    result[n] = source[x in taken ? taken[x] : x];
    taken[x] = --len;
  }

  return result;
};


export const setPageTitle = (title) => {
  if (title) {
    document.title = title;
  }
};


const EMBLEM_URL = template `https://gatherer.wizards.com/Handlers/Image.ashx?type=symbol&set=${0}&size=${1}&rarity=${2}`;

export const mapToGathererSet = (scryfallSet) => {
  const set = scryfallSet.toLowerCase();
  return mappingScryfallWithGatherer[set] || set;
};


export const getEmblemURL = (set, rarity, size = 'small') => {
  let rarityChar = '';
  switch(rarity) {
    case 'common':
      rarityChar = 'C';
      break;
    case 'uncommon':
      rarityChar = 'U';
      break;
    case 'rare':
      rarityChar = 'R';
      break;
    case 'mythic':
      rarityChar = 'M';
      break;
    default:
      rarityChar = 'C';
      break;
  }

  if (set.length === 4 && set.startsWith('t')) {
    set = set.substring(1);
  }
  let gathererSet = mapToGathererSet(set);

  if (set === 'dde') {
    size = 'small';
  } else if (set === 'med') {
    rarityChar = 'M';
  }

  return EMBLEM_URL(gathererSet, size, rarityChar);
};


export const getSymbolList = (manaCost) => {
  if (!manaCost) {
    return [];
  }

  return manaCost.replace(/}{/g, ' ').split(' ').map(
    symbol => symbol.replace(/[{}/]/g, '')
  );
};


// See more icons here: https://andrewgioia.github.io/Keyrune/icons.html
const mappingScryfallWithGatherer = {
  'gk1': null,  // GRN Guildkit, check for 'watermark'
  'gk2': null,  // RNA Guildkit, check for 'watermark'
  'ana': null,  // Arena New Player Experience
  'med': 'MPS_GRN',  // Mythic Edition
  'g17': null,  // 2017 Gift Pack
  'prm': null,  // Magic Online Promos
  'mps': 'MPS_KLD',  // Masterpiece Series: Kaladesh Inventions
  'jvc': 'DD3_JVC',  // Duel Decks Anthology: Jace vs. Chandra
  'gvl': 'DD3_GVL',  // Duel Decks Anthology: Garruk vs. Liliana
  'dvd': 'DD3_DVD',  // Duel Decks Anthology: Divine vs. Demonic
  'j14': null,  // Judge Gift Cards 2014
  'td0': null,  // Magic Online Theme Decks
  'dpa': null,  // Duels of the Planeswalkers
  'dd1': 'EVG',  // Duel Decks: Elves vs. Goblins
  'me1': 'MED',  // Master Edition
  'cst': null,  // Coldsnap Theme Decks
  'dkm': null,  // Deckmasters
  'ody': 'OD',  // Odyssey
  'apc': 'AP',  // Apocalypse
  '7ed': '7E',  // Seventh Edition
  'pls': 'PS',  // Planeshift
  'inv': 'IN',  // Invasion
  'btd': 'BD',  // Beatdown Box Set
  'pcy': 'PR',  // Prophecy
  's00': 'P4',  // Starter 2000
  'nem': 'NE',  // Nemesis
  'brb': 'BR',  // Battle Royale Box Set
  'mmq': 'MM',  // Mercadian Masques
  's99': 'P3',  // Starter 1999
  'uds': 'CG',  // Urza's Destiny
  'ptk': 'PK',  // Portal Three Kingdoms
  '6ed': '6E',  // Classic Sixth Edition
  'ulg': 'GU',  // Urza's Legacy
  'ath': null,  // Anthology
  'usg': 'UZ',  // Urza's Saga
  'ugl': 'UG',  // Unglued
  'exo': 'EX',  // Exodus
  'p02': 'P2',  // Portal Second Age
  'sth': 'ST',  // Stronghold
  'tmp': 'TE',  // Tempest
  'wth': 'WL',  // Weatherlight
  'por': 'PO',  // Portal
  '5ed': '5E',  // Fifth Edition
  'vis': 'VI',  // Visions
  'itp': null,  // Introductory Two-Player Set
  'mir': 'MI',  // Mirage
  'rqs': null,  // Rivals Quick Start Set
  'all': 'AL',  // Alliances
  'ptc': null,  // Pro Tour Collector Set
  'hml': 'HM',  // Homelands
  'rin': null,  // Rinascimento
  'chr': 'CH',  // Chronicles
  'ice': 'IA',  // Ice Age
  '4bb': null,  // Fourth Edition Foreign Black Border
  '4ed': '4E',  // Fourth Edition
  'fem': 'FE',  // Fallen Empires
  'drk': 'DK',  // The Dark
  'sum': null,  // Summer Magic / Edgar
  'leg': 'LE',  // Legends
  '3ed': '3E',  // Revised Edition
  'fbb': null,  // Foreign Black Border
  'atq': 'AQ',  // Antiquities
  'arn': 'AN',  // Arabian Nights
  'cei': null,  // Intl. Collectors’ Edition
  'ced': null,  // Collectors’ Edition
  '2ed': '2U',  // Unlimited Edition
  'leb': '2E',  // Limited Edition Beta
  'lea': '1E',  // Limited Edition Alpha
}