export const SORT_TYPES = {
  CMC: 'cmc',
  PRICE: 'price',
  COLOR: 'color',
  NAME: 'name',
  TYPE: 'type',
  EXP: 'expansion',
  INDEX: 'index',
  TIME: 'time',
};
const { CMC, COLOR, PRICE, NAME, TYPE, EXP, INDEX, TIME } = SORT_TYPES;

const basicColors = ['W', 'U', 'B', 'R', 'G'];

/**
 * @description Evaluate the given color identity list.
 * @param {Array<String>} colors - The color identity list, most of the time manacost
 * @return {Number} The evaluated number
 */
const calculateManaPriority = colors => {
  if (colors.length === 0) {
    return basicColors.length + 1;
  }

  return colors.reduce(
    (totalValue, currentColor) => {
      const index = basicColors.indexOf(currentColor);
      if (index !== -1) {
        totalValue += index + 1;
      } else {
        totalValue += basicColors.length + 1;
      }
      return totalValue;
    }, 0
  );
};

const getSortValue = (item, criteria) => {
  switch(criteria) {
    case CMC:
      return item.card.cmc;
    case COLOR:
      return calculateManaPriority(item.card.colors);
    case PRICE:
      const { foil, normal } = item.card.tcgPrices;
      return item.foil ? foil.market : normal.market;
    case NAME:
      return item.card.name;
    case TYPE:
      return item.card.typeLine;
    case EXP:
      return item.card.set;
    case INDEX:
      return item.card.collectorNumber;
    case TIME:
      return item.card.releasedAt;
    default:
      return item.card.cmc;
  };
};

/**
 * @description Compare all records in an array with prioritized criterias.
 * @param {Array<Card>} cards - The card list
 * @param {Array<String>} criterias - The prioritized criteria lists
 * @todo Change criterias into an Array<Object> of { criteria: String, isAscending: Boolean }
 */
export const extremelySortCards = (cards, criterias, isAscending = true) => {
  let result = cards.sort(
    (a, b) => {
      let comparisonResult = 0;
      let index = 0;

      while (!comparisonResult) {
        let _sortValueA = getSortValue(a, criterias[index]);
        let _sortValueB = getSortValue(b, criterias[index]);

        if (typeof _sortValueA === 'number') {
          comparisonResult = _sortValueA - _sortValueB;
        } else if (typeof _sortValueA === 'string') {
          comparisonResult = _sortValueA.localeCompare(_sortValueB);
        } else {
          comparisonResult = _sortValueA - _sortValueB;
        }

        index ++;
        if (index >= criterias.length) {
          break;
        }
      }

      return (isAscending ? comparisonResult : -comparisonResult);
    }
  );

  return result;
};